Gestionar
==========

```node server.js```: Ejecuta la aplicación

> Gestionar es un conjunto de aplicaciones destinadas a mejorar la gestión

Tecnologías
-----------

+ [Angular.JS](http://www.angularjs.org/)
+ [Node.JS](http://www.nodejs.org)
+ [MongoDB](http://www.mongodb.org)
+ [Express.js](http://expressjs.com/)

Organización
------------

La aplicación está organizada por módulos que residen bajo la subcarpeta modules.


```
...gestionar.../
...gestionar.../source
...gestionar.../source/modules
```

Implementación de un módulo
---------------------------

Cada modulo contiene los siguientes archivos:


```
app.js // Nombre del módulo y enlace a aparecer en la pagina principal
ngmodules.js
permissions.js // Listado de permisos del módulo
resources.js // Listado de las colecciones en la bd del módulo
routes.js // Listado de las rutas del módulo
scripts.js // Listado de todos los archivos .js que son parte del módulo
```

### Configuración

#### Revisar config.json
Ver el archivo `config.json`.
Es necesario:

- Una instancia de MongoDB:

  - Autenticada (recomendado):

```
"mongo": {
    "database": "nombredelabasededatos",
    "hostname": "nombre-del-servidor",
    "auth": true,
    "username": "usuario",
    "password": "clave",
    "autoreconnect": true,
    "port": 27017
}
```

  - No autenticada (desarrollo):  

```
"mongo": {
    "database": "bag2",
    "hostname": "localhost",
    "auth": false,
    "username": "",
    "password": "",
    "autoreconnect": true,
    "port": 27017
}
```

- Clave para cookies y sesión:

```
    "cookies": {
        "secret": "1234567890"
    },
    "session": {
        "secret": "1234567890",
        "key": "express.sid"
    }
```

- Puerto en el que se va a ejecutar:

```
"http": {
        "port": 80
    }
```

