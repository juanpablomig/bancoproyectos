Manual de instalación Gestionar RHEL
==========

Instalar Node Js
-----------

1) ```yum clean && yum update```

2) ```yum remove -y nodejs npm```

3) ```curl -- silent --location https://rpm.nodesource.com/setup_6.x | bash -```

4) ```yum install -y nodejs```


Instalar MongoDB como Servicio
-----------

1) ```vim /etc/yum.repos.d/mongodb-org-3.4.repo```
```
[mongodb-org-3.4]
name = MongoDB Repository
baseurl = https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck = 0
enabled = 1
gpgkey = https://www.mongodb.org/static/pgp/server-3.4.asc
```

2) ```sudo yum install -y mongodb-org```

3) Parado en root ejecutar ```mkdir /data```

4) Dentro de /data crear la carpeta /db: ```mkdir db``` (el usuario que utilice el proceso de Mongo debe poseer permisos de rwx)


Instalar y ejecutar Urbano
-----------

1) Clonar el repositorio:

```git clone http://git-asi.buenosaires.gob.ar/usuarioQA/asi-167-sistemamdu.git```

2) Instalar el modulo forever: ```npm install -g forever```

Esto es necesario para luego poder correr el server como un servicio.

3) Parado en la carpeta /source ejecutar ```npm install```.

4) Crear el usuario inicial ejecutando en consola: ```node saveUser.js dev dev admin.users admin.db```.

Este usuario es un superuser para poder crear desde él otros usuarios y asignar permisos.

Puede modificarse su contraseña y configuración desde la zona de administración (click en el nombre de usuario/zona de administración).

De no funcionar el código anterior insertar directamente en la base de datos lo siguiente:

	- Iniciar la consola de mongo ejecutando mongo.
	
	- Seleccionar la base de datos a usar: use bag2
	
	- Insertar un usuario:	```db.users.insert({username:"dev",password:"sha1$b5bfe8d1$1$aaf7c3fe67f9253831d73f9018a5e8c91b2d542a"})```

	- Agregar permisos:	```db.users.permissions.insert({username:"dev",permissions:["admin.users","admin.db"]})```

5) Dentro de la carpeta /source crear la carpeta /uploads. 

En esta carpeta se guardarán todos los archivos que se suban ( fotos, videos, pdf, etc)

6) En una terminal iniciar el servicio de mongo con: sudo service mongod start.

	- Si el servicio no levanta revisar el archivo /etc/mongod.config , comentar la linea bind_ip= 127.0.0.1 y volver a	intentar iniciarlo.
	
7) Parado en la carpeta /source levantar el servidor ejecutando desde consola: ```forever start server.js```.



Notas:

Si está activado iptables para bloquear todo el tráfico, es prácticamente lo primero que tienen que revisar cuando no llegan a un puerto de forma externa pero internamente el servidor responde. 

Cómo se fija uno que el servidor está escuchando el puerto?

```curl http://localhost/``` (desde el servidor) muestra el HTML en la consola

Reglas del firewall anteriores:
-----------

```
[root@deve-sistemaurbano-01 ~]# iptables -S
-P INPUT ACCEPT
-P FORWARD ACCEPT
-P OUTPUT ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT 
-A INPUT -p icmp -j ACCEPT 
-A INPUT -i lo -j ACCEPT 
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT 
-A INPUT -j REJECT --reject-with icmp-host-prohibited 
-A INPUT -p tcp -m tcp --dport 80 -j ACCEPT 
-A INPUT -i lo -j ACCEPT 
-A FORWARD -j REJECT --reject-with icmp-host-prohibited 
```

Borro loa que molesta:

```iptables -D INPUT -j REJECT --reject-with icmp-host-prohibited```

Agrego una que deje pasar el puerto 80:

```iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT```

Vuelvo a agregar la vieja al final de la lista:

```iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited```
