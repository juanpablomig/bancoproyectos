function DBCollection(user, db, collectionName) {
  this.user = user;
  this.db = db;
  this.collectionName = collectionName;
};

DBCollection.prototype.find = function (predicate, sort, projection) {
  return new Promise(
    (resolve, reject) => {
      try {
        this.db.collection(this.collectionName).find(predicate || {}, projection || {})
          .sort(sort)
          .toArray(function (err, docs) {
            if (err) return reject(err);

            resolve(docs);
          });
      } catch (e) {
        reject(e);
      }
    }
  );
};

DBCollection.prototype.findOne = function (predicate) {
  return new Promise(
    (resolve, reject) => {

      try {
        this.db.collection(this.collectionName).findOne(predicate || {}, function (err, doc) {
          if (err) return reject(err);

          resolve(doc);
        });
      } catch (e) {
        reject(e);
      }
    }
  );
};

DBCollection.prototype.insert = function (newObject) {
  return new Promise(
    (resolve, reject) => {
      try {
        this.db.collection(this.collectionName).insert(newObject || {}, function (err, doc) {
          if (err) return reject(err);

          //FIXME: new format
          if (typeof (newObject) === 'object') return resolve(doc[0]);

          return resolve(doc);
        });
      } catch (e) {
        reject(e);
      }
    }
  );
};

DBCollection.prototype.update = function (predicate, replacement) {
  return new Promise(
    (resolve, reject) => {
      try {
        this.db.collection(this.collectionName).update(predicate || {}, replacement || {}, function (err) {
          if (err) return reject(err);

          return resolve();
        });
      } catch (e) {
        reject(e);
      }
    }
  );
};

DBCollection.prototype.remove = function (predicate) {
  return new Promise(
    (resolve, reject) => {
      try {
        this.db.collection(this.collectionName).insert(predicate || {}, function (err) {
          if (err) return reject(err);

          return resolve();
        });
      } catch (e) {
        reject(e);
      }
    });
};

function DBApi(user, db) {
  this.user = user;
  this.db = db;
};

DBApi.prototype.collection = function (collectionName) {
  return new DBCollection(this.user, this.db, collectionName);
};

module.exports = DBApi;
