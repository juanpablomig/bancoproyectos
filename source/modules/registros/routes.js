exports = module.exports = {
    '/registros/empresas': {
        title: 'Registros',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/empresas.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            },
            'navbar-extra-right@': {
                templateUrl: '/views/registros/navbarRight.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros/individuales': {
        title: 'Registros',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/individuales.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            },
            'navbar-extra-right@': {
                templateUrl: '/views/registros/navbarRight.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros/nuevo': {
        title: 'Registros Individuales - Nuevo',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/nuevo.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros/:_id': {
        title: 'Registros Individuales - Detalle',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/nuevo.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros/empresas/nuevo': {
        title: 'Registros Empresas - Nuevo',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/nuevoEmpresa.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros/empresas/:_id': {
        title: 'Registros Empresas - Detalle',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/nuevoEmpresa.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros': {
        title: 'Registros',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            },
            'navbar-extra-right@': {
                templateUrl: '/views/registros/navbarRight.html'
            }
        },
        reloadOnSearch: false,
    },
    '/registros/imprimir/:_id': {
        title: 'Registros - Imprimir',
        section: 'registros',
        allowed: ['registros'],
        views: {
            'body@': {
                templateUrl: '/views/registros/imprimir.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/registros/navbar.html'
            }
        },
        reloadOnSearch: false,
    }
};