angular.module('bag2.registros', [])
.controller('RegistrosCtrl', function($scope, Registro) {
	$scope.cvs = Registro.query();

	$scope.aMilisegundos = function(fecha) {
		var fechaDividida = fecha.split("/");
		var date = new Date(fechaDividida[2], fechaDividida[1] - 1, fechaDividida[0], 0, 0, 0, 0);
		return date.getTime();
	};

	$scope.fechaActual = new Date().getTime();
	$scope.annoEnMili = 31556900000;

	$scope.filtroFin = function(ba) {
		var edadMinima = $scope.edadMinima * $scope.annoEnMili;
		var compararFechaMinima = $scope.fechaActual - edadMinima;
		var edadMaxima = $scope.edadMaxima * $scope.annoEnMili;
		var compararFechaMaxima = $scope.fechaActual - edadMaxima;
		if ($scope.edadMinima && $scope.edadMaxima) {
			if (ba.fechaNacimientoMiliseg <= compararFechaMinima && ba.fechaNacimientoMiliseg >= compararFechaMaxima) {
				return true;
			} else {
				return false;
			}
		} else if ($scope.edadMinima) {
			if (ba.fechaNacimientoMiliseg <= compararFechaMinima) {
				return true;
			} else {
				return false;
			}
		} else if ($scope.edadMaxima) {
			if (ba.fechaNacimientoMiliseg >= compararFechaMaxima) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	};
})
.controller('RegistrosEmpresasCtrl', function($scope, RegistroEmpresa) {
	$scope.cvs = RegistroEmpresa.query();

	$scope.aMilisegundos = function(fecha) {
		var fechaDividida = fecha.split("/");
		var date = new Date(fechaDividida[2], fechaDividida[1] - 1, fechaDividida[0], 0, 0, 0, 0);
		return date.getTime();
	};

	$scope.fechaActual = new Date().getTime();
	$scope.annoEnMili = 31556900000;

	$scope.filtroFin = function(ba) {
		var edadMinima = $scope.edadMinima * $scope.annoEnMili;
		var compararFechaMinima = $scope.fechaActual - edadMinima;
		var edadMaxima = $scope.edadMaxima * $scope.annoEnMili;
		var compararFechaMaxima = $scope.fechaActual - edadMaxima;
		if ($scope.edadMinima && $scope.edadMaxima) {
			if (ba.fechaNacimientoMiliseg <= compararFechaMinima && ba.fechaNacimientoMiliseg >= compararFechaMaxima) {
				return true;
			} else {
				return false;
			}
		} else if ($scope.edadMinima) {
			if (ba.fechaNacimientoMiliseg <= compararFechaMinima) {
				return true;
			} else {
				return false;
			}
		} else if ($scope.edadMaxima) {
			if (ba.fechaNacimientoMiliseg >= compararFechaMaxima) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	};
})
.controller('RegistrosNuevoCtrl', function($scope, Registro, $routeParams, $location) {

	var camposEditando = function(estado) {
		$('input').prop("disabled", estado);
		$('select').prop("disabled", estado);
		$('textarea').prop("disabled", estado);
	};

	$scope.editando = false;

	if($routeParams._id) {
		$scope.cv = Registro.get({
			_id: $routeParams._id
		});
		camposEditando(true);
	} else {
		$scope.cv = {};
	}
	$scope.uploaded = [];

	$scope.aMilisegundos = function(fecha) {
		var fechaDividida = fecha.split("/");
		var date = new Date(fechaDividida[2], fechaDividida[1] - 1, fechaDividida[0], 0, 0, 0, 0);
		return date.getTime();
	};

	$scope.editar = function() {
		$scope.editando = true;
		camposEditando(false);
	};

	$scope.eliminar = function(confirmado) {
		if (confirmado) {
			$('#confirmarEliminar').modal('hide');	
			$('#confirmarEliminar').on('hidden.bs.modal', function () {
				$scope.cv.$delete(function() {
					$location.url('/registros');
				});	
			});
		} else {
			$("#confirmarEliminar").modal('toggle');
		}
	};

	$scope.guardar = function() {
		if ($scope.uploaded.length) {
			$scope.cv.archivoId = $scope.uploaded.shift().id;
		}
		if ($scope.cv.fechaNacimiento) {
			$scope.cv.fechaNacimientoMiliseg = $scope.aMilisegundos($scope.cv.fechaNacimiento);
		} else {
			$scope.cv.fechaNacimientoMiliseg = 0;
		}

		if($routeParams._id) {
			$scope.cv.$save();
		} else {
			var cv = new Registro($scope.cv);
			cv.$save();
		}
	};

	$scope.agregarIdioma = function(valorIdioma) {
		if (!$scope.cv.idiomas) {
			$scope.cv.idiomas = [];
		}

		if ($scope.cv.idiomas.length <= 5) {
			$scope.cv.idiomas.push(valorIdioma);
		} else {
			alert("Ya ingreso todos los posibles");
		}

		$scope.valorIdioma = "";
	};

	$scope.agregarSistema = function(valorSistema) {
		if (!$scope.cv.sistemas) {
			$scope.cv.sistemas = [];
		}

		$scope.cv.sistemas.push(valorSistema);

		$scope.valorSistemas = "";
	};

	$scope.agregarEstudios = function(valorEst) {
		if (!$scope.cv.estudios) {
			$scope.cv.estudios = [];
		}
		$scope.cv.estudios.push(valorEst);
		$scope.estudios = "";
	};

	$scope.agregarReferencia = function(valorReferencia) {
		if (!$scope.cv.referencias) {
			$scope.cv.referencias = [];
		}

		$scope.cv.referencias.push(valorReferencia);

		$scope.referencias = "";
	};

	$scope.agregarExperiencia = function(valorExp) {
		if (!$scope.cv.experiencias) {
			$scope.cv.experiencias = [];
		}
		$scope.cv.experiencias.push(valorExp);
		$scope.valorExperiencia = "";
	};

	$scope.eliminarIdioma = function(elemento, lista) {
		lista.splice(lista.indexOf(elemento), 1);
	};
})
.controller('RegistrosNuevoEmpresaCtrl', function($scope, RegistroEmpresa, $routeParams, $location) {

	var camposEditando = function(estado) {
		$('input').prop("disabled", estado);
		$('select').prop("disabled", estado);
		$('textarea').prop("disabled", estado);
	}

	$scope.editando = false;

	if($routeParams._id) {
		$scope.cv = RegistroEmpresa.get({
			_id: $routeParams._id
		});
		camposEditando(true);
	} else {
		$scope.cv = {};
	}
	$scope.uploaded = [];

	$scope.aMilisegundos = function(fecha) {
		var fechaDividida = fecha.split("/");
		var date = new Date(fechaDividida[2], fechaDividida[1] - 1, fechaDividida[0], 0, 0, 0, 0);
		return date.getTime();
	};

	$scope.editar = function() {
		$scope.editando = true;
		camposEditando(false);
	};

	$scope.eliminar = function(confirmado) {
		if (confirmado) {
			$('#confirmarEliminar').modal('hide');	
			$('#confirmarEliminar').on('hidden.bs.modal', function () {
				$scope.cv.$delete(function() {
					$location.url('/registros');
				});	
			});
		} else {
			$("#confirmarEliminar").modal('toggle');
		}
	};

	$scope.guardar = function() {
		if ($scope.uploaded.length) {
			$scope.cv.archivoId = $scope.uploaded.shift().id;
		}
		if ($scope.cv.fechaNacimiento) {
			$scope.cv.fechaNacimientoMiliseg = $scope.aMilisegundos($scope.cv.fechaNacimiento);
		} else {
			$scope.cv.fechaNacimientoMiliseg = 0;
		}

		if($routeParams._id) {
			$scope.cv.$save();
		} else {
			var cv = new RegistroEmpresa($scope.cv);
			cv.$save();
		}
	};

	$scope.agregarIdioma = function(valorIdioma) {
		if (!$scope.cv.idiomas) {
			$scope.cv.idiomas = [];
		}

		if ($scope.cv.idiomas.length <= 5) {
			$scope.cv.idiomas.push(valorIdioma);
		} else {
			alert("Ya ingreso todos los posibles");
		}

		$scope.valorIdioma = "";
	};

	$scope.agregarSistema = function(valorSistema) {
		if (!$scope.cv.sistemas) {
			$scope.cv.sistemas = [];
		}

		$scope.cv.sistemas.push(valorSistema);

		$scope.valorSistemas = "";
	};

	$scope.agregarEstudios = function(valorEst) {
		if (!$scope.cv.estudios) {
			$scope.cv.estudios = [];
		}
		$scope.cv.estudios.push(valorEst);
		$scope.estudios = "";
	};

	$scope.agregarReferencia = function(valorReferencia) {
		if (!$scope.cv.referencias) {
			$scope.cv.referencias = [];
		}

		$scope.cv.referencias.push(valorReferencia);

		$scope.referencias = "";
	};

	$scope.agregarExperiencia = function(valorExp) {
		if (!$scope.cv.experiencias) {
			$scope.cv.experiencias = [];
		}
		$scope.cv.experiencias.push(valorExp);
		$scope.valorExperiencia = "";
	};

	$scope.eliminarIdioma = function(elemento, lista) {
		lista.splice(lista.indexOf(elemento), 1);
	};
})
.controller('RegistrosImprimirCtrl', function($scope, Registro, $routeParams, $window) {
	$scope.cv = Registro.get({
		_id: $routeParams._id
	});

	$scope.imprimir = function() {
		$window.print();
	};
});