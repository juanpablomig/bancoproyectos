exports = module.exports = [{
    name: 'Registro',
    collectionName: 'registros',
    url: '/registros/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/registros'],
            allowed: ['registros'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne'
        },
        save: {
            urls: ['/registros','/registros/:_id'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove'
        }
    }
},{
    name: 'RegistroEmpresa',
    collectionName: 'registrosEmpresas',
    url: '/registrosEmpresas/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/registrosEmpresas'],
            allowed: ['registros'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne'
        },
        save: {
            urls: ['/registrosEmpresas','/registrosEmpresas/:_id'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove'
        }
    }
}];