exports = module.exports = [{
  name: 'Administración - Administrar usuarios',
  key: 'admin.users'
},{
  name: 'Administración - Reportes',
  key: 'admin.reportes'
},{
  name: 'Administración - Acceso BD',
  key: 'admin.db'
}];