exports = module.exports = {
  name: 'Usuarios',
  orden: 99,
  url: '/admin/users',
  permission: 'admin.users'
};