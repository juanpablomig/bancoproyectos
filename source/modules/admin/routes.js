exports = module.exports = {
    '/admin': {
        section: 'admin',
        allowed: ['admin.users'],
        views: {
            'body@': {
                templateUrl: '/views/admin/admin.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/admin/navbar.html'
            }
        }
    },
    '/registrarse': {
        section: 'inicio',
        views: {
            'body@': {
                templateUrl: '/views/admin/users/registrarse.html'
            }
        }
    },
    '/olvideContrasena': {
        section: 'inicio',
        views: {
            'body@': {
                templateUrl: '/views/admin/users/olvideContrasena.html'
            }
        }
    },
    '/admin/users': {
        section: 'users',
        allowed: ['admin.users'],
        title: 'Administrar usuarios',
        parents: ['/admin'],
        views: {
            'body@': {
                templateUrl: '/views/admin/users/list.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/admin/navbar.html'
            }
        }
    },
    '/admin/users/:username': {
        section: 'users',
        allowed: ['admin.users'],
        title: 'Modificar usuario',
        parents: ['/admin', '/admin/users'],
        views: {
            'body@': {
                templateUrl: '/views/admin/users/edit.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/admin/navbar.html'
            }
        }
    },
    '/admin/users/:username/editarDatos': {
        section: 'users',
        allowed: ['admin.db'],
        title: 'Editar Datos',
        parents: ['/admin', '/admin/users'],
        views: {
            'body@': {
                templateUrl: '/views/admin/users/editarDatos.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/admin/navbar.html'
            }
        }
    },
    '/admin/users/:username/changePassword': {
        section: 'users',
        allowed: ['admin.users'],
        title: 'Cambiar contraseña',
        parents: ['/admin', '/admin/users'],
        views: {
            'body@': {
                templateUrl: '/views/admin/users/changePassword.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/admin/navbar.html'
            }
        }
    },
    '/cambiarContrasenna': {
        title: 'Cambiar Contraseña',
        section: 'cambiarContrasenna',
        views: {
            'body@': {
                templateUrl: '/views/admin/cambiarContrasenna/index.html'
            }
        },
        reloadOnSearch: false,
    }
};