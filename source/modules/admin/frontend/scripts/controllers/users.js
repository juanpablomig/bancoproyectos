'use strict';

angular.module('bag2.admin.users',[])
.controller('UsersCtrl', function($scope, User, UserPermission) {
    $scope.users = User.query();
    $scope.elemento = {};
    $scope.usuarioPermiso = "";
    
    $scope.eliminar = function(confirmado, u) {
        if (confirmado) {
            $scope.elemento.$delete(function() {
                $scope.users = User.query();
            });
        }
        else {
            $scope.elemento = u;
            $("#confirmarEliminar").modal('show');
        }
    };
    
    $scope.copiarPermisos = function(confirmado, u) {
        if (confirmado) {
            $scope.usu = UserPermission.query(function() {
                $scope.usu.forEach(function(item) {
                    if(item.username == $scope.usuarioPermiso){
                        $scope.elemento.permissions = item.permissions;
                        $scope.elemento.$save();
                    }
                });
            });
        }
        else {
            $scope.elemento = UserPermission.findByName({
                username: u.username
            }, function() {
                if (!$scope.elemento.permissions) $scope.elemento.permissions = [];
            });
            $("#copiarPermi").modal('show');
        }
    };
    
    $scope.$on('new-user', function() {
        $scope.users = User.query();
    });
    
    $scope.valorCorreo = function(c) {
        if (c.correos) {
            for (var i = 0; i < c.correos.length; i++) {
                if (c.correos[i].nombre == 'Email oficial') {
                    return c.correos[i].valor;
                }
            }
            return '';
        }
    };
    
    $scope.fTrim = function (str) {
        var cadena = "";
        if (str) {
            for (var i = 0; i < str.length; i++) {
                if (str.charAt(i)=="@") {
                    cadena=str.substring(0,i);
                }
            }
        }
        return cadena;
    };
    
}).controller('NewUserCtrl', function($scope, $http, $location, User, UserPermission) {
    $scope.valorCorreo = function(c) {
        if (c.correos) {
            for (var i = 0; i < c.correos.length; i++) {
                if (c.correos[i].nombre == 'Email oficial') {
                    return c.correos[i].valor;
                }
            }
            return '';
        }
    };
    
    $scope.fTrim = function (str) {
        var cadena = "";
        if (str) {
            for (var i = 0; i < str.length; i++) {
                if (str.charAt(i)=="@") {
                    cadena=str.substring(0,i);
                }
            }
        }
        return cadena;
    };
    $scope.save = function() {
        if ($scope.user.username) {
            $scope.user.interno = true;
            $scope.user.$save(function() {
                if ($scope.newPassword) {
                    $http.post('/api/admin/changePassword', {
                        username: $scope.user.username,
                        newPassword: $scope.newPassword
                    });
                }
                $scope.user = new User();
                $scope.$emit('new-user');
            });
        } else {
            alert("Debe ingresar un nombre de usuario");
        }
    };
    
    $scope.saveRegistro = function() {
        if ($scope.user.username && $scope.user.nombre && $scope.user.apellidos && $scope.user.email) {
            if ($scope.newPassword == $scope.newPassword2) {
                $scope.userPermissions = new UserPermission({
                    username: $scope.user.username,
                    permissions: [
                        "solicitudes",
                        "solicitudes.solicitante",
                        "solicitudes.db",
                        "ofertas.db",
                        "municipios.db",
                        "admin.db",
                        "configuracion.db"
                    ]
                });
                $scope.user.interno = false;
                $scope.user.$save(function() {
                    if ($scope.newPassword) {
                        $http.post('/api/admin/changePassword', {
                            username: $scope.user.username,
                            newPassword: $scope.newPassword
                        });
                    }
                    $scope.userPermissions.$save();
                    $location.url('/login');
                });
            } else {
                alert("Las contraseñas no coinciden");
            }
        } else {
            alert("Faltan ingresar datos");
        }
    };
	
	$scope.mailContrasena = function() {
        var adjunto="";
		
		$scope.payload = {
			asunto: "SOFEP - Recuperación de contraseña",
			para: "juampablucho@gmail.com",
			mensajeHtml: "<div class='mailwrapper' style='font-size: 15px;'>SOFEP<br>Nos llego el aviso que olvidaste tu método de acceso.<br><hr><br>Configura una nueva contraseña haciendo <a href='http://sofep.com.ar/admin/users'>Click Aquí</a>",      
			from : "SOFEP <norespondersofep@upcefe.org.ar>",
			adjunto: adjunto
		};
		
		$http.post('/api/notificacion/enviar-mail', $scope.payload).success(function() {
			alert("Mail enviado correctamente");
		}).error(function() {
			alert("Fallo el envio");
		});
	};
	


    
    $scope.user = new User();
}).controller('EditUserCtrl', function($scope, $http, User, UserPermission, $location, Permission, $routeParams) {
    var self = this;

    self.allPermissions = [];
    $http.get('/api/allPermissions').success(function (allPermissions) {
       self.allPermissions = allPermissions; 
       for(var i=0; i<self.allPermissions.length; i++)
       {
           self.allPermissions[i].group = self.allPermissions[i].name.split("-", 1)[0].trim();
       }
    });
    $scope.$watch('filtro.group', function (valor){
        
        if (valor) 
        {
            $scope.variable=true;
        }
        else
        {
            $scope.variable=false;
        }
    });
    
    $scope.tocaron=function(event)
    {
        if($scope.filtro.name.length>0)
        {
            $scope.variable=true;
        }
        else
        {
            $scope.variable=false;
        }
    };
    
    var anterior="";
    $scope.seRepite=function(permiso)
    {
        if(permiso.group!=anterior)
        {
            anterior=permiso.group;
            return true;
        }
        else
        {
            anterior=permiso.group;
            return false;
        }
    };
    
    $scope.segundaParte=function(valor)
    {
        if(valor.indexOf("-")==-1)
        {
            return valor;
        }
        else
        {
            return valor.split("-",2)[1];
        }
    };
    
    $scope.user = User.findByName({
        username: $routeParams.username
    });
    $scope.userPermissions = UserPermission.findByName({
        username: $routeParams.username
    }, function() {
        if (!$scope.userPermissions.permissions) $scope.userPermissions.permissions = [];
    });
    $scope.deshabilitar = function () {
        $scope.userPermissions.permissions = [];
    };
    $scope.save = function () {
        // $scope.user.$save();
        $scope.userPermissions.$save();
        $location.url('/admin/users');
    };
}).controller('EditUserPermissionsCtrl', function($scope) {
    $scope.remove = function(p) {
        $scope.userPermissions.permissions.splice($scope.userPermissions.permissions.indexOf(p), 1);
    };
}).controller('AddPermissionCtrl', function($scope,UserPermission) {
    var self = this;
    
    $scope.tienePermiso = function(usuario, key) {
        try{
            for(var i = 0; i < $scope.userPermissions.permissions.length; i++) {
                if ($scope.userPermissions.permissions[i] == key){
                    return true;
                }
            }  
        }
        catch(Exception){
            
        }
        return false;
    };
    
    $scope.cambiarPermiso = function(usuario, key) {
        if(($scope.userPermissions.username == "admin") && key == "admin.users"){
            alert("No se pueden modificar los permisos del usuario Admin");
        }else{
            if ($scope.tienePermiso(usuario, key)) {
                $scope.userPermissions.permissions.splice($scope.userPermissions.permissions.indexOf(key), 1);
            } else {
                $scope.userPermissions.permissions.push(key);
            }
        }
    };
    
    $scope.comprobarPermisos = function(permisosRol){
        var total = 0;
        for(var r=0;r<permisosRol.length;r++){
            if($scope.userPermissions.permissions) {
                for(var p=0;p<$scope.userPermissions.permissions.length;p++) {
                    if(permisosRol[r] == $scope.userPermissions.permissions[p]){
                        total = total + 1;
                    }
                }
            }            
        }
        if(total == permisosRol.length){
            return true;
        }else{
            return false;
        }
    };

    $scope.cambiarRol = function(permisosRol,prioridad){
        if($scope.comprobarPermisos(permisosRol)){
            //Si tiene los permisos que corresponden al rol los saco
            permisosRol.forEach(function(rol){
                $scope.userPermissions.permissions.splice($scope.userPermissions.permissions.indexOf(rol), 1);
            })
        }else{
            //Sino los agrego pero solo los que no estan
            permisosRol.forEach(function(rol){
                var encontrado = false;
                for (var p = 0;p < $scope.userPermissions.permissions.length; p++) {
                    if(rol == $scope.userPermissions.permissions[p]){
                        encontrado = true;
                        break;
                    }
                }
                if(!encontrado){
                    $scope.userPermissions.permissions.push(rol);
                }
            })
        }
    };

    $scope.filtroRol = function(permiso){
        if(permiso.rol){
            return true;
        }else{
            return false;
        }
    };

    $scope.filtroPermiso = function(permiso){
        if(!permiso.rol){
            return true;
        }else{
            return false;
        }
    };
    
    $scope.tienePermisoRol = function(roles){
        var total = 0;
        for(var r=0;r<roles.length;r++){
            if($scope.userPermissions.permissions) {
                for(var p=0;p<$scope.userPermissions.permissions.length;p++) {
                    if(roles[r] == $scope.userPermissions.permissions[p]){
                        total = total + 1;
                    }
                }
            }
        }
        if(total == roles.length){
            return true;
        }else{
            return false;
        }
    };

    $scope.permisosUsuarios = UserPermission.query();

    $scope.sacar = function(cadena){
        var a = cadena.indexOf("-");
        return cadena.substr(a + 2,cadena.length);
    }

    $scope.cambiarPermisoNuevo = function(usuario, key) {
        console.log(usuario)
        if(($scope.userPermissions.username == "admin") && key == "admin.users"){
            alert("No se pueden modificar los permisos del usuario Admin");
        }else{
            if ($scope.tienePermiso2(usuario, key)) {
                usuario.permissions.splice(usuario.permissions.indexOf(key), 1);
                usuario.$save();
            } else {
                usuario.permissions.push(key);
                usuario.$save();
            }
        }
    };

    $scope.tienePermiso2 = function(usuario, key) {
        try{
            for(var i = 0; i < usuario.permissions.length; i++) {
                if (usuario.permissions[i] == key){
                    return true;
                }
            }  
        }
        catch(Exception){
            
        }
        return false;
    };
}).controller('ChangePasswordCtrl', function($scope, User, $location, $routeParams, $http) {
    $scope.user = User.findByName({
        username: $routeParams.username
    });
    $scope.save = function () {
        if($scope.user.username.length){
            $http.post('/api/admin/changePassword', {
                username: $routeParams.username,
                newPassword: $scope.newPassword
            })
            .success(function() {
                /* $scope.alerts.push({
                    type:'success',
                    message: 'Se cambió la contraseña correctamente'
                }); */
                $location.url('/admin/users');
            })
            .error(function() {
                /* $scope.alerts.push({
                    type:'success',
                    message: 'Se cambió la contraseña correctamente'
                }); */
            });
        }
    };
}).controller('EditarDatosCtrl', function($scope, User, $location, $routeParams) {
    $scope.user = User.findByName({
        username: $routeParams.username
    });
    
    $scope.save = function () {
        $scope.user.$save({}, function() {
            if ($scope.hasPermission('admin.users')) {
                $location.url('/admin/users');
            } else {
                $location.url('/');
            }
        });
    };
});
