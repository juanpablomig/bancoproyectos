exports = module.exports = function(app, conf, db) {
	var emails = require('../../emails/backend/locals');

	var enviarMail = function(asunto, listaPara, textoHtml, from, adjunto, callback) {
		var para = listaPara;
		var cc = '';
		var cco = '';
		var html = textoHtml;
		var subject = asunto;
		var texto = require('html-to-text').fromString(html, {
			wordwrap: 130
		});

		var emailMessage = emails.createMessage(from, subject, texto, html, para, cc, cco, adjunto, '');

		emails.sendMail(emailMessage, callback);
	};

	app.post(conf.api.prefix + '/notificacion/enviar-mail', function(req, res) {
		var payload = req.body;

		enviarMail(payload.asunto, payload.para, payload.mensajeHtml, payload.from, payload.adjunto,
			function(err) {
				if (err) {
					console.log(err);
					res.status(503);
					res.end();
				}
				else {
					res.json({});
				}
			});
	});
};