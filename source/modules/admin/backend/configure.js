exports= module.exports = function (app, conf, db) {
  require('./changePassword')(app, conf, db);  
  
  // configurar la api para enviar mails
  require('./email.js')(app, conf);

  app.get('/backup-db.tar', function(req, res, next) {
        var backup = require('mongodb-backup');
        
        backup({
          uri: conf.db, 
          stream: res,
          collections: ["users",
                        "users.permissions",
                        "fs.chunks",
                        "fs.files"],
          callback: function (err) {
              if (err) return next(err);
              
              res.end();
          }
        });
  });
};