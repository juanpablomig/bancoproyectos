angular.module('bag2.reportes', ['nvd3'])
.controller("ReportePrincipalCtrl", function ($scope) {
	$scope.tab = "ofertas";
	
})
.controller('ReportesOfertasCtrl', function($scope, $location, Oferta, OfertaOrganismo) {
    $scope.cargando = true;
    $scope.editando = false;
    $scope.totalMonto = 0;
    $scope.totalProgramas = 0;
    
    $scope.soloNumeros = function(cadenaAnalizar) {
        if (cadenaAnalizar) {
            var nuevoString = "0";
            for (var i = 0; i< cadenaAnalizar.length; i++) {
                var caracter = cadenaAnalizar.charAt(i);
                if ( caracter == "0" || caracter == "1" || caracter == "2" || caracter == "3" || caracter == "4" || caracter == "5" || caracter == "6" || caracter == "7" || caracter == "8" || caracter == "9") {
                    nuevoString = nuevoString + caracter;
                }
            }
            return parseInt(nuevoString);
        } else {
            return 0;
        }
    };
    
    $scope.ofertas = Oferta.query({}, function() {
      $scope.organismos = OfertaOrganismo.query({}, function() {
        $scope.organismos.forEach(function(o) {
          o.total = 0;
          o.cantidad = 0;
          $scope.ofertas.forEach(function(i) {
              if (i.organismo == o._id) {
                o.total = o.total + $scope.soloNumeros(i.saldoValor);
                o.cantidad = o.cantidad + 1;
                $scope.totalMonto = $scope.totalMonto + $scope.soloNumeros(i.saldoValor);
                $scope.totalProgramas = $scope.totalProgramas + 1;
              }
          });
        });
        $scope.data = [];
        $scope.armarGrafico($scope.data);
        $scope.data2 = [];
        $scope.armarGrafico2($scope.data2);
        $scope.cargando = false;
      });
    });
    
    $scope.options = {
            chart: {
                type: 'discreteBarChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 50,
                    left: 55
                },
                x: function(d){return d.label;},
                y: function(d){return d.value;},
                showValues: true,
                duration: 500,
                xAxis: {
                    axisLabel: 'Organismos'
                },
                yAxis: {
                    axisLabel: '',
                    axisLabelDistance: -10
                }
            }
        };
    
    $scope.armarGrafico = function(datos) {
        var data = {
            key: "Organismos",
            values: []
        };
        for (var h = 0; h < $scope.organismos.length; h++) {
            data.values.push({
                label: $scope.organismos[h].nombre,
                value: parseInt($scope.organismos[h].total) || 0
            });
        }
        datos.push(data);
    };
    
    $scope.armarGrafico2 = function(datos) {
        var data = {
            key: "Organismos",
            values: []
        };
        for (var h = 0; h < $scope.organismos.length; h++) {
            data.values.push({
                label: $scope.organismos[h].nombre,
                value: parseInt($scope.organismos[h].cantidad) || 0
            });
        }
        datos.push(data);
    };
    
    
    
    //DEVUELVE UN NUMERO CON LOS PUNTOS DE MILES
    $scope.formatMiles = function(nStr) {
        if (nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        } else {
            return "";
        }
    };

})
.controller('ReportesSolicitudesCtrl', function($scope, $location, Solicitud) {
    $scope.cargando = true;
    $scope.editando = false;
    $scope.totalMonto = 0;
    $scope.totalSolicitudes = 0;
    $scope.estados = [{
      nombre : 'Sin Oferta'
    },{
      nombre : 'Solicitado'
    },{
      nombre : 'Observado'
    },{
      nombre : 'Pendiente'
    },{
      nombre : 'Completado'
    }];
    
    $scope.soloNumeros = function(cadenaAnalizar) {
        if (cadenaAnalizar) {
          if (typeof myVar == 'string') {
            var nuevoString = "0";
            for (var i = 0; i< cadenaAnalizar.length; i++) {
                var caracter = cadenaAnalizar.charAt(i);
                if ( caracter == "0" || caracter == "1" || caracter == "2" || caracter == "3" || caracter == "4" || caracter == "5" || caracter == "6" || caracter == "7" || caracter == "8" || caracter == "9") {
                    nuevoString = nuevoString + caracter;
                }
            }
            return parseInt(nuevoString);
          } else {
            return cadenaAnalizar;
          }
        } else {
            return 0;
        }
    };
    
    $scope.solicitudes = Solicitud.query({}, function() {
        $scope.estados.forEach(function(o) {
          o.total = 0;
          o.cantidad = 0;
          $scope.solicitudes.forEach(function(i) {
              if (i.estado == o.nombre) {
                o.total = o.total + $scope.soloNumeros(i.costo);
                o.cantidad = o.cantidad + 1;
                $scope.totalMonto = $scope.totalMonto + $scope.soloNumeros(i.costo);
                $scope.totalSolicitudes = $scope.totalSolicitudes + 1;
              }
          });
        });
        $scope.data = [];
        $scope.armarGrafico($scope.data);
        $scope.data2 = [];
        $scope.armarGrafico2($scope.data2);
        $scope.cargando = false;
    });
    
    $scope.options = {
        chart: {
            type: 'discreteBarChart',
            height: 450,
            margin : {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function(d){return d.label;},
            y: function(d){return d.value;},
            showValues: true,
            duration: 500,
            xAxis: {
                axisLabel: 'Estados'
            },
            yAxis: {
                axisLabel: '',
                axisLabelDistance: -10
            }
        }
    };
    
    $scope.options2 = {
        chart: {
            type: 'pieChart',
            height: 500,
            x: function(d){return d.key;},
            y: function(d){return d.y;},
            showLabels: true,
            duration: 5,
            labelThreshold: 0.01,
            labelSunbeamLayout: true,
            legend: {
                margin: {
                    top: 5,
                    right: 35,
                    bottom: 5,
                    left: 0
                }
            }
        }
    };
    
    $scope.armarGrafico = function(datos) {
        var data = {
            key: "Estados",
            values: []
        };
        for (var h = 0; h < $scope.estados.length; h++) {
            data.values.push({
                label: $scope.estados[h].nombre,
                value: parseInt($scope.estados[h].total) || 0
            });
        }
        datos.push(data);
    };
    
    $scope.armarGrafico2 = function(datos) {
        for (var h = 0; h < $scope.estados.length; h++) {
            datos.push({
                key: $scope.estados[h].nombre,
                y: parseInt($scope.estados[h].cantidad) || 0
            });
        }
    };
    
    
    
    //DEVUELVE UN NUMERO CON LOS PUNTOS DE MILES
    $scope.formatMiles = function(nStr) {
        if (nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        } else {
            return "";
        }
    };
    
})
.controller('ReporteTiempoCtrl', function($scope, UserTick, User, $window) {
    $scope.users = User.query();
    $scope.userTicks = UserTick.query();
    $scope.orden = 'desdeDate';
    
    $scope.dameTiempo = function (hasta, desde) {
        var horas = Math.floor((hasta - desde) / 3600000);
        var minutos = Math.floor(((hasta - desde) % 3600000) / 60000);
        if (minutos < 10) {
            return horas + "hs 0" + minutos + "min";
        } else {
            return horas + "hs " + minutos + "min";
        }
    };
    
    $scope.dameFecha = function (desde) {
        var fecha = new Date(desde);
        var devolver = fecha.getDate() + "/";
        if ((fecha.getMonth() + 1) < 10) {
            devolver = devolver + "0" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " a las " + fecha.getHours() + ":";
        } else {
            devolver = devolver + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " a las " + fecha.getHours() + ":";
        }
        if (fecha.getMinutes() < 10) {
            devolver = devolver + "0" + fecha.getMinutes() + "hs";
        } else {
            devolver = devolver + fecha.getMinutes() + "hs";
        }
        return devolver;
    };
    
    $scope.imprimir = function () {
        $window.print(); 
    };

})
.controller('ReporteMapaCtrl', function($scope, $modal, $filter, Municipio, Oferta, Solicitud) {
    $scope.localidades = Municipio.query();
    $scope.organigrama = [];
    
    $scope.filtro = {
        _id : "",
        solicitudes : "",
        ofertas : ""
    };
    
    $scope.seleccionado = {};
    $scope.haySeleccionado = false;
    var marcadores = [];
    
    $scope.ofertas = Oferta.query({}, function() {
        $scope.solicitudes = Solicitud.query({}, function() {
            $scope.localidades = Municipio.query({}, function() {
                $scope.map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -40.250291863006375, lng: -67.12992577332294}, 
                    zoom: 6.5
                });
                $scope.localidades.forEach(function(h) {
                    h.solicitudes = [];
                    h.ofertas = [];
                    $scope.ofertas.forEach(function(p) {
                        if (p.municipios) {
                            if ((p.municipios.indexOf(h._id) > -1) || (p.municipios.indexOf("60897a8d8d10f83545f3afc6") > -1)) {
                                h.ofertas.push(p._id);
                            }
                        }
                    });
                    $scope.solicitudes.forEach(function(p) {
                        if (p.municipios) {
                            if ((p.municipios.indexOf(h._id) > -1) || (p.municipios.indexOf("60897a8d8d10f83545f3afc6") > -1)) {
                                h.solicitudes.push(p._id);
                            }
                        }
                    });
                    if (h.lat) {
                        agregarMarker(h);
                    }
                });
            });
        });
    });
    
    $scope.mostrarTexto = function (localidad) {
        $scope.filtro._id = localidad._id;
        $("#s3").select2('val',localidad._id);
        $scope.seleccionado = localidad;
        $scope.haySeleccionado = true;
        $scope.hayOfertaSeleccionado = false;
        $scope.haySolicitudSeleccionada = false;
        $scope.$apply();
    };
    
    $scope.mostrarCuadro = function (localidad) {
        $scope.cuadro = localidad;
        $scope.haySeleccionadoCuadro = true;
        $scope.$apply();
    };
    
    $scope.noMostrarCuadro = function () {
        $scope.cuadro = {};
        $scope.haySeleccionadoCuadro = false;
        $scope.$apply();
    };
    
    $scope.solicitudPorId = function (id) {
        for (var i = 0; i < $scope.solicitudes.length; i++) {
            if ($scope.solicitudes[i]._id == id) {
                return $scope.solicitudes[i];
            }
        }  
    };
    
    $scope.ofertaPorId = function (id) {
        for (var i = 0; i < $scope.ofertas.length; i++) {
            if ($scope.ofertas[i]._id == id) {
                return $scope.ofertas[i];
            }
        }  
    };
    
    $scope.municipioPorId = function (id) {
        for (var i = 0; i < $scope.localidades.length; i++) {
            if ($scope.localidades[i]._id == id) {
                return $scope.localidades[i];
            }
        }  
    };
    
    $scope.filtroOfertas = function (id) {
        if ($scope.filtro.solicitudes === "") {
            return true;
        } else {
            for (var i = 0; i < $scope.ofertas.length; i++) {
                if ($scope.ofertas[i]._id == id) {
                    return ($scope.ofertas[i].idSolicitud == $scope.filtro.solicitudes);
                }
            }
        }
    };
    
    $scope.filtroOfertas2 = function (proy) {
        if ($scope.filtro.solicitudes === "") {
            return true;
        } else {
            for (var i = 0; i < $scope.ofertas.length; i++) {
                if ($scope.ofertas[i]._id == proy._id) {
                    return ($scope.ofertas[i].idSolicitud == $scope.filtro.solicitudes);
                }
            }
        }
    };
    
    $scope.filtroOfertas3 = function (proy) {
        if ($scope.filtro._id === "") {
            return true;
        } else {
            return ($scope.municipioPorId($scope.filtro._id).ofertas.indexOf(proy._id) > -1);
        }
    };
    
    $scope.filtroJuris = function (juris) {
        if ($scope.filtro.ofertas === "") {
            return true;
        } else {
            for (var i = 0; i < $scope.ofertas.length; i++) {
                if ($scope.ofertas[i]._id == $scope.filtro.ofertas) {
                    return ($scope.ofertas[i].idSolicitud == juris._id);
                }
            }
        }
    };
    
    $scope.filtroJuris2 = function (juris) {
        if ($scope.filtro._id === "") {
            return true;
        } else {
            return ($scope.municipioPorId($scope.filtro._id).solicitudes.indexOf(juris._id) > -1);
        }
    };
    
    $scope.filtroLocali = function (loca) {
        if ($scope.filtro.ofertas === "") {
            return true;
        } else {
            return (loca.ofertas.indexOf($scope.filtro.ofertas) > -1);
        }
    };
    
    $scope.filtroLocali2 = function (loca) {
        if ($scope.filtro.solicitudes === "") {
            return true;
        } else {
            return (loca.solicitudes.indexOf($scope.filtro.solicitudes) > -1);
        }
    };
    
    $scope.limpiarFiltros = function () {
        $scope.hayOfertaSeleccionado = false;
        $scope.haySolicitudSeleccionada = false;
        $scope.haySeleccionado = false;
        $scope.filtro = {
            _id : "",
            solicitudes : "",
            ofertas : ""
        };
        $("#s1").select2('val',"");
        $("#s2").select2('val',"");
        $("#s3").select2('val',"");
        $("#s4").select2('val',"");
        $scope.localidades.forEach(function(h) {
            if (h.lat) {
                agregarMarker(h);
            }
        });
    };
    
    $scope.seleccionoOferta = function () {
        if ($scope.filtro.ofertas !== "") {
            $scope.haySeleccionado = false;
            $scope.haySolicitudSeleccionada = false;
            $scope.hayOfertaSeleccionado = true;
            $scope.ofertaSeleccionado = $scope.ofertaPorId($scope.filtro.ofertas);
        } else {
            $scope.hayOfertaSeleccionado = false;
        }
    };
    
    $scope.seleccionoSolicitud = function () {
        if ($scope.filtro.solicitudes !== "") {
            $scope.haySeleccionado = false;
            $scope.hayOfertaSeleccionado = false;
            $scope.haySolicitudSeleccionada = true;
            $scope.solicitudSeleccionada = $scope.solicitudPorId($scope.filtro.solicitudes);
        } else {
            $scope.haySolicitudSeleccionada = false;
        }
    };
    
    $scope.filtrar = function () {
        for (var i = 0; i < marcadores.length; i++) {
            marcadores[i].setMap(null);
        }
        marcadores = [];
        if ($scope.filtro._id === "") {
            $filter('filter')($scope.localidades, $scope.filtro).forEach(function(h) {
                if (h.lat) {
                    agregarMarker(h);
                }
            });
        } else {
            var h = $scope.municipioPorId($scope.filtro._id);
            $scope.mostrarTexto(h);
            if ($scope.filtro.ofertas === "") {
                if ($scope.filtro.solicitudes === "") {
                    if (h.lat) {
                        agregarMarker(h);
                    }
                } else if (h.solicitudes.indexOf($scope.filtro.solicitudes) > -1) {
                    if (h.lat) {
                        agregarMarker(h);
                    }
                }
            } else if ($scope.filtro.solicitudes === "") {
                if ($scope.filtro.ofertas === "") {
                    if (h.lat) {
                        agregarMarker(h);
                    }
                } else if (h.ofertas.indexOf($scope.filtro.ofertas) > -1) {
                    if (h.lat) {
                        agregarMarker(h);
                    }
                }
            }
        }
    };
        
    var agregarMarker = function (h) {
        var pinColor = "9ACA3C";
        var titulo = h.nombre;
        var punto = new google.maps.Marker({
            position: {lat: parseFloat(h.lat), lng: parseFloat(h.lng)},
            strokeColor: '#'+pinColor,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#'+pinColor,
            fillOpacity: 0.35,
            map: $scope.map,
            title: titulo,
            zIndex: 2
        });
        punto.addListener('click', function() {
            $scope.mostrarTexto(h);
        });
        punto.addListener('mouseover', function() {
            $scope.mostrarCuadro(h);
        });
        punto.addListener('mouseout', function() {
            $scope.noMostrarCuadro();
        });
        marcadores.push(punto);
    };
  
    $scope.verDetalle = function(oferta) {
        $scope.ofertaSeleccionado = $scope.ofertaPorId(oferta);
        $modal({template: '/views/reportes/modals/verDetalle.html', persist: true, show: true, size: 'modal-lg', scope: $scope.$new()});
    };
    
    $scope.verDetalleSolicitud = function(solicitud) {
        $scope.solicitudSeleccionado = $scope.solicitudPorId(solicitud);
        $modal({template: '/views/reportes/modals/verDetalle.html', persist: true, show: true, size: 'modal-lg', scope: $scope.$new()});
    };
    
});