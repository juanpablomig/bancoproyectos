exports = module.exports = {
    '/reportes': {
        title: 'Reporte Ejecutivo',
        section: 'reportes',
        allowed: ['reportes'],
        views: {
            'body@': {
                templateUrl: '/views/reportes/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/reportes/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/reportes/mapa': {
        title: 'Reporte Ejecutivo - Mapa',
        section: 'mapa',
        allowed: ['reportes'],
        views: {
            'body@': {
                templateUrl: '/views/reportes/mapa.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/reportes/navbar.html'
            }
        },
        reloadOnSearch: false,
    }
};