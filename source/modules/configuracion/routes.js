exports = module.exports = {
    '/configuracion': {
        title: 'Configuración',
        section: 'configuracion',
        allowed: ['configuracion'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/configuracion/municipios': {
        title: 'Municipios',
        section: 'municipios',
        allowed: ['configuracion'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/municipios/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/configuracion/municipios/nuevo': {
        title: 'Nuevo Municipio',
        section: 'municipios',
        reloadOnSearch: false,
        allowed: ['configuracion'],
        parents: ['/municipios'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/municipios/nuevo.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        }
    },
    '/configuracion/municipios/:_id': {
        title: 'Detalle Municipio',
        section: 'municipios',
        reloadOnSearch: false,
        allowed: ['configuracion'],
        parents: ['/municipios'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/municipios/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        }
    },
    '/configuracion/jurisdicciones': {
        title: 'Jurisdiccion',
        section: 'jurisdicciones',
        allowed: ['configuracion'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/jurisdicciones/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/configuracion/jurisdicciones/:_id': {
        title: 'Detalle',
        section: 'jurisdicciones',
        reloadOnSearch: false,
        allowed: ['configuracion'],
        parents: ['/jurisdicciones'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/jurisdicciones/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        }
    },
    '/configuracion/ofertas': {
        title: 'Ofertas',
        section: 'ofertas',
        allowed: ['configuracion'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/ofertas/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/configuracion/organismosProvinciales': {
        title: 'Organismos Provinciales',
        section: 'organismosProvinciales',
        allowed: ['configuracion'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/organismosProvinciales/index.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        },
        reloadOnSearch: false,
    },
    '/configuracion/organismosProvinciales/:_id': {
        title: 'Detalle',
        section: 'organismosProvinciales',
        reloadOnSearch: false,
        allowed: ['configuracion'],
        parents: ['/organismosProvinciales'],
        views: {
            'body@': {
                templateUrl: '/views/configuracion/organismosProvinciales/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/configuracion/navbar.html'
            }
        }
    }
};