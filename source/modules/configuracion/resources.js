exports = module.exports = [{
    name: 'Municipio',
    collectionName: 'municipios',
    url: '/municipios/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/municipios'],
            allowed: ['configuracion.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne'
        },
        save: {
            urls: ['/municipios','/municipios/:_id'],
            allowed: ['configuracion.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['configuracion.db']
        }
    }
}];