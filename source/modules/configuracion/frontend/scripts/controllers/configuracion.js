angular.module('bag2.configuracion', [])
.controller('MunicipiosCtrl', function($scope, Municipio, $location) {
    $scope.municipios = Municipio.list();
    $scope.ver = function(c) {
        $location.url('/configuracion/municipios/' + c._id);
    };
}).controller('NuevoMunicipioCtrl', function($scope, Municipio, $routeParams, $location) {
    $scope.municipio = new Municipio ();
    $scope.guardar = function() {
        $scope.municipio.$save(function() {
            $location.url('/configuracion/municipios/' + $scope.municipio._id);
        });  
    };
}).controller('VerMunicipioCtrl', function($scope, Municipio, $routeParams, $location) {
    $scope.municipio = Municipio.get({_id: $routeParams._id});
    $scope.modificando = false;
    $scope.modificar = function () {
        $scope.modificando = true;
    };
    $scope.cancelar = function () {
        $scope.municipio = Municipio.get({_id: $routeParams._id});
        $scope.modificando = false;
    };
    $scope.eliminar = function() {
        if (confirm("Esta seguro de eliminar este municipio?")) {
            $scope.municipio.$delete(function() {
                $location.url('/configuracion/municipios/');
            });  
        }
    };
    $scope.guardar = function() {
        $scope.municipio.$save(function() {
            $scope.modificando = false;
        });  
    };
    $scope.puedeModificar = function() {
            return true;
    };
})
.controller('JurisdiccionesCtrl', function($scope, OfertaJurisdiccion, $location) {
    $scope.jurisdicciones = OfertaJurisdiccion.query({eliminado: JSON.stringify({$exists: false})});
    $scope.ver = function(c) {
        $location.url('/configuracion/jurisdicciones/' + c._id);
    };
       
    $scope.eliminarJurisdiccion = function (jurisdiccion) {
        if (confirm("¿Esta seguro de borrar esta jurisdiccion?")) {
            jurisdiccion.eliminado = true;
            jurisdiccion.$save({}, function() {
                $scope.jurisdicciones = OfertaJurisdiccion.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
    $scope.nuevoJuri = '';
    $scope.crearJurisdiccion = function () {
        var regi = new OfertaJurisdiccion ({
            nombre : $scope.nuevoJuri
        });
        regi.$save({}, function() {
            $scope.jurisdicciones = OfertaJurisdiccion.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoJuri = '';
        });
    };
}).controller('VerJurisdiccionCtrl', function($scope, OfertaJurisdiccion, $routeParams, $location) {
    $scope.jurisdiccion = OfertaJurisdiccion.get({_id: $routeParams._id});
    $scope.modificando = false;
    $scope.modificar = function () {
        $scope.modificando = true;
    };
    $scope.cancelar = function () {
        $scope.jurisdiccion = OfertaJurisdiccion.get({_id: $routeParams._id});
        $scope.modificando = false;
    };
    $scope.guardar = function() {
        $scope.jurisdiccion.$save(function() {
            $scope.modificando = false;
        });  
    };
    $scope.puedeModificar = function() {
            return true;
    };
})
.controller('OrganismosProvincialesCtrl', function($scope, SolicitudOrganismoProvincial, $location) {
    $scope.organismosProvinciales = SolicitudOrganismoProvincial.query({eliminado: JSON.stringify({$exists: false})});
    $scope.ver = function(c) {
        $location.url('/configuracion/organismosProvinciales/' + c._id);
    };
       
    $scope.eliminarOrganismo = function (organismo) {
        if (confirm("¿Esta seguro de borrar este organismo?")) {
            organismo.eliminado = true;
            organismo.$save({}, function() {
                $scope.organismosProvinciales = SolicitudOrganismoProvincial.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
    $scope.nuevoOrga = '';
    $scope.crearOrganismo = function () {
        var regi = new SolicitudOrganismoProvincial ({
            nombre : $scope.nuevoOrga
        });
        regi.$save({}, function() {
            $scope.organismosProvinciales = SolicitudOrganismoProvincial.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoOrga = '';
        });
    };
}).controller('VerOrganismoProvincialCtrl', function($scope, SolicitudOrganismoProvincial, $routeParams, $location) {
    $scope.organismo = SolicitudOrganismoProvincial.get({_id: $routeParams._id});
    $scope.modificando = false;
    $scope.modificar = function () {
        $scope.modificando = true;
    };
    $scope.cancelar = function () {
        $scope.organismo = SolicitudOrganismoProvincial.get({_id: $routeParams._id});
        $scope.modificando = false;
    };
    $scope.guardar = function() {
        $scope.organismo.$save(function() {
            $scope.modificando = false;
        });  
    };
    $scope.puedeModificar = function() {
            return true;
    };
})
//CONTROLLER DE LA SECCION CONFIGURACION
.controller("OfertaConfiguracionCtrl", function ($scope, OfertaOrganismo, OfertaOperacion, OfertaGasto, OfertaPrograma, OfertaSector, OfertaObjetivo, OfertaComponente) {

    $scope.gastos = OfertaGasto.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoGast = '';
    $scope.crearGasto = function () {
        var gast = new OfertaGasto ({
            nombre : $scope.nuevoGast
        });
        gast.$save({}, function() {
            $scope.gastos = OfertaGasto.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoGast = '';
        });
    };
    $scope.eliminarGasto = function (gasto) {
        if (confirm("¿Esta seguro de borrar este gasto?")) {
            gasto.eliminado = true;
            gasto.$save({}, function() {
                $scope.gastos = OfertaGasto.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
    $scope.organismos = OfertaOrganismo.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoOrga = '';
    $scope.crearOrganismo = function () {
        var regi = new OfertaOrganismo ({
            nombre : $scope.nuevoOrga
        });
        regi.$save({}, function() {
            $scope.organismos = OfertaOrganismo.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoOrga = '';
        });
    };
    $scope.eliminarOrganismo = function (organismo) {
        if (confirm("¿Esta seguro de borrar esta organismo?")) {
            organismo.eliminado = true;
            organismo.$save({}, function() {
                $scope.organismos = OfertaOrganismo.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
    $scope.operaciones = OfertaOperacion.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoOper = '';
    $scope.crearOperacion = function () {
        var regi = new OfertaOperacion ({
            nombre : $scope.nuevoOper
        });
        regi.$save({}, function() {
            $scope.operaciones = OfertaOperacion.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoOper = '';
        });
    };
    $scope.eliminarOperacion = function (operacion) {
        if (confirm("¿Esta seguro de borrar esta operacion?")) {
            operacion.eliminado = true;
            operacion.$save({}, function() {
                $scope.operaciones = OfertaOperacion.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
    $scope.programas = OfertaPrograma.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoProg = '';
    $scope.crearPrograma = function () {
        var regi = new OfertaPrograma ({
            nombre : $scope.nuevoProg
        });
        regi.$save({}, function() {
            $scope.programas = OfertaPrograma.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoProg = '';
        });
    };
    $scope.eliminarPrograma = function (programa) {
        if (confirm("¿Esta seguro de borrar esta programa?")) {
            programa.eliminado = true;
            programa.$save({}, function() {
                $scope.programas = OfertaPrograma.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
    $scope.sectores = OfertaSector.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoSect = '';
    $scope.crearSector = function () {
        var regi = new OfertaSector ({
            nombre : $scope.nuevoSect
        });
        regi.$save({}, function() {
            $scope.sectores = OfertaSector.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoSect = '';
        });
    };
    $scope.eliminarSector = function (sector) {
        if (confirm("¿Esta seguro de borrar este sector?")) {
            sector.eliminado = true;
            sector.$save({}, function() {
                $scope.sectores = OfertaSector.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
    $scope.objetivos = OfertaObjetivo.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoObje = '';
    $scope.crearObjetivo = function () {
        var regi = new OfertaObjetivo ({
            nombre : $scope.nuevoObje
        });
        regi.$save({}, function() {
            $scope.objetivos = OfertaObjetivo.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoObje = '';
        });
    };
    $scope.eliminarObjetivo = function (objetivo) {
        if (confirm("¿Esta seguro de borrar esta objetivo?")) {
            objetivo.eliminado = true;
            objetivo.$save({}, function() {
                $scope.objetivos = OfertaObjetivo.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
    $scope.componentes = OfertaComponente.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoComp = '';
    $scope.crearComponente = function () {
        var regi = new OfertaComponente ({
            nombre : $scope.nuevoComp
        });
        regi.$save({}, function() {
            $scope.componentes = OfertaComponente.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoComp = '';
        });
    };
    $scope.eliminarComponente = function (componente) {
        if (confirm("¿Esta seguro de borrar esta componente?")) {
            componente.eliminado = true;
            componente.$save({}, function() {
                $scope.componentes = OfertaComponente.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
  
});