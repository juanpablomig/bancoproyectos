exports = module.exports = [{
  name: 'Solicitudes - Acceso',
  key: 'solicitudes'
},{
  name: 'Solicitudes - Edición',
  key: 'solicitudes.editar'
},{
  name: 'Solicitudes - Acceso BD',
  key: 'solicitudes.db'
},{
  name: 'Solicitudes - Solicitante',
  key: 'solicitudes.solicitante'
},{
  name: 'Solicitudes - Administrador',
  key: 'solicitudes.admin'
},{
  name: 'Solicitudes - Eliminar',
  key: 'solicitudes.eliminar'
}];