exports = module.exports = [{
  name: 'Solicitudes de Financiamiento',
  orden: 2,
  url: '/solicitudes',
  permission: 'solicitudes.editar'
},{
  name: 'Mis Solicitudes',
  orden: 3,
  url: '/solicitudes',
  permission: 'solicitudes.solicitante'
}];