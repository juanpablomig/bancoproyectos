exports = module.exports = [{
    name: 'Solicitud',
    collectionName: 'solicitudes',
    url: '/solicitudes/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/solicitudes'],
            allowed: ['solicitudes.db'],
            kind: 'find'
        },
        findByName: {
            kind: 'findOne',
            allowed: ['solicitudes.db']
        },
        save: {
            urls: ['/solicitudes','/solicitudes/:_id'],
            allowed: ['solicitudes.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['solicitudes']
        }
    }
},
{
    name: 'SolicitudOrganismoProvincial',
    collectionName: 'solicitudes.organismosProvinciales',
    url: '/solicitudes.organismosProvinciales/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/solicitudes.organismosProvinciales'],
            //allowed: ['solicitudes.db'],
            kind: 'find'
        },
        findByName: {
            kind: 'findOne',
            allowed: ['solicitudes.db']
        },
        save: {
            urls: ['/solicitudes.organismosProvinciales','/solicitudes.organismosProvinciales/:_id'],
            allowed: ['solicitudes.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['solicitudes']
        }
    }
}];