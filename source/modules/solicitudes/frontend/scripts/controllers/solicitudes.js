angular.module('bag2.solicitudes', [])
.controller('SolicitudCtrl', function($scope, $routeParams, Solicitud, $location, Municipio, Oferta, OfertaPrograma) { //Lista de solicitudes
    
    if ($scope.hasPermission('solicitudes.solicitante')) {
        $scope.solicitudes = Solicitud.query({username: $scope.username});
    } else {
        $scope.solicitudes = Solicitud.query();
    }
    //Listado de solicitudes cargados
    
    
    $scope.municipios = Municipio.query();
    $scope.ofertas = Oferta.query();
    $scope.programas = OfertaPrograma.query();
    
    $scope.municipioPorId = function (id) {
        for (var i = 0; i < $scope.municipios.length; i++) {
            if ($scope.municipios[i]._id == id) {
                return $scope.municipios[i].nombre;
            }
        }  
    };
    
    var programaPorId = function (id) {
        for (var i = 0; i < $scope.programas.length; i++) {
            if ($scope.programas[i]._id == id) {
                return $scope.programas[i].nombre;
            }
        }  
    };
    
    $scope.ofertaPorId = function (id) {
        for (var i = 0; i < $scope.ofertas.length; i++) {
            if ($scope.ofertas[i]._id == id) {
                return programaPorId($scope.ofertas[i].programa);
            }
        }
    };
    
    $scope.colorEstado = function(i){
        if (!i.estado && !i.oferta) {
            return {'background-color':'#999'};
        } else {
            if (i.estado == 'Completado') {
                return {'background-color':'#5cb85c'};
            } else if (i.estado == 'Observado') {
                return {'background-color':'#f0ad4e'};
            } else if (i.estado == 'Pendiente') {
                return {'background-color':'#d9534f'};
            } else if (i.estado == 'Solicitado') {
                return {'background-color':'#5bc0de'};
            } else if (i.estado == 'Sin Oferta') {
                return {'background-color':'#999'};
            }
        }
    };
    
    $scope.colorLinea = function(i){
        if (!i.estado && !i.oferta) {
            return {'border-bottom':'3px solid #999'};
        } else {
            if (i.estado == 'Completado') {
                return {'border-bottom':'3px solid #5cb85c'};
            } else if (i.estado == 'Observado') {
                return {'border-bottom':'3px solid #f0ad4e'};
            } else if (i.estado == 'Pendiente') {
                return {'border-bottom':'3px solid #d9534f'};
            } else if (i.estado == 'Solicitado') {
                return {'border-bottom':'3px solid #5bc0de'};
            } else if (i.estado == 'Sin Oferta') {
                return {'border-bottom':'3px solid #999'};
            }
        }
    };

    
    $scope.exportToExcel=function(){
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "text/html; charset=UTF-8"
        });
        saveAs(blob, "Solicitudes.xls");
    };
    
    $scope.verSolicitud = function(id) {
        $location.path('/solicitudes/detalle/' + id);
    };
    
    $scope.reordenar = function(texto) {
        if (texto == $scope.orden)
            return ('-' + texto);
        else
            return texto;
    };
    
})//CONTROLLER DE LA SECCION CONFIGURACION
.controller("SolicitudConfiguracionCtrl", function ($scope) {
    /*$scope.regiones = SolicitudRegion.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoRegi = {
        nombre:''
    };
  
    //CREAR Region
    $scope.crearRegion = function () {
        var regi = new SolicitudRegion ({
            nombre : $scope.nuevoRegi.nombre
        });
        regi.$save({}, function() {
            $scope.regiones = SolicitudRegion.query({eliminado: JSON.stringify({$exists: false})});
            $scope.nuevoRegi = {
                nombre:''
            };
        });
    };
  
    //ELIMINAR Region
    $scope.eliminarRegion = function (region) {
        if (confirm("¿Esta seguro de borrar esta región?")) {
            region.eliminado = true;
            region.$save({}, function() {
                $scope.regiones = SolicitudRegion.query({eliminado: JSON.stringify({$exists: false})});
            });
        }
    };
    
    $scope.segmentos = SolicitudSegmento.query({eliminado: JSON.stringify({$exists: false})});
    $scope.nuevoSegme = {
        nombre:''
    };*/
  
})
.controller('SolicitudDetalleCtrl',function($scope,$location,$routeParams,API,User,Solicitud,SolicitudOrganismoProvincial, Oferta, OfertaJurisdiccion, OfertaPrograma, OfertaOperacion, OfertaOrganismo, Municipio){

    $scope.denominacion = "Es el nombre con el que se lo designa. Se sugiere que incluya el proceso (construcción, repavimentación, acondicionamiento, etc.), el objeto (Acueducto; Ruta Nacional xx; instalaciones portuarias) y localización (entre tal y tal localidad; en la ciudad de Posadas; etc). Eventualmente se puede añadir el servicio que presta la obra (Ramal para transporte de cargas; infraestructura de provisióno de servicios básicos, etc).";
    $scope.problema = "La puesta en servicio de la obra permite resolver problemas tales como la falta de acceso a una localidad, los altos costos logísticos para la producción local, las deficientes condiciones de vida de los pobladores, etc. Suponen que hay grupos afectados que se benefician con su solución o (beneficiarios).Se trata aquí de enunciar y describir este problema que justifica la realizacióno de la inversióno y constituye su “razóno de ser”. A veces es posible medir el problema, es decir, la discrepancia entre la situacióno actual y aquella en la que se aspira a estar. Por ejemplo “los costos de logística de ésta zona de la provincia son un x% mayores que el promedio.";
    $scope.descripcion = "Se describen brevemente los elementos incluidos en la Denominación (proceso, objeto, localización y servicio.";
    $scope.provincia = "Provincia en la que se ubica la obra";
    $scope.municipio = "Nombre del municipio";
    $scope.beneficios = "Los beneficios son aquellos resultados que se espera lograr en la modificación del problema. Generalmente se expresan en términos de modificación de una variable o aspecto de la realidad. También es conveniente especificar la población que se beneficiará. Por ejemplo: con la reparacióno y reactivacióno de la línea x del ferrocarril se logrará reducir en un x% los costos logísticos para los no productores de la zona x que podráno trasladar sus productos a través del mismo";
    $scope.organismo = "Es el nombre del organismo formalmente responsable de la ejecución del proyecto.";
    $scope.tipo = "Se refiere a una categorización predefinida entre proyectos de infraestructura, infraestructura productiva, urbana, sanitaria y educativa. Ver la lista de códigos en el clasificador adjunto. Se excluyen pequeñas obras.";
    $scope.estadoProyecto = "Se refiere a la fase en la que el proyecto se encuentra en relación con su implementación efectiva. También se responde con categorías predefinidas y codificadas. Idea, diseño, Proyecto técnico, iniciado (interrumpido)";
    $scope.solicitudFina = "Se refiere la disponibilidad y origen de los recursos para financiar el proyecto. También con categorías predefinidas. No dispone de financiamiento, Dispone de financiamiento internacional, Dispone de financiamiento provincial total. Dispone de financiamiento provincial parcial.";
    $scope.organismoF = "Es el organismo específico que aporta fondos cuando fuera el caso. (FFFIR; Programa de Desarrollo de Parques Industriales del Ministerio de Desarrollo Productivo, Programa BID 4753 Ministerio del Interior, etc.)";
    $scope.costoRecursos = "En dólares";
    $scope.tiempo = "El tiempo, medido en meses, que se estima demandará la obra a partir de su inicio y hasta su entrega final.";
    $scope.prioridad = "Es la valorización relativa de la máxima autoridad política respecto de la relevancia del proyecto para la estrategia de desarrollo provincial.";
    $scope.impacto = "Permite medir impactos de Proyecto, pudiendo orientar las decisiones relativas a la apropiación del costo";
    $scope.adjunto = [];
    $scope.adjuntoPriorizada = [];
    $scope.ofertasAsignadas = [];
    $scope.listaInversiones = ["Consultoría o asistencia técnica", "Equipamiento: equipamiento tecnológico", "Equipamiento: bienes de capital", "Equipamiento: mobiliario", "Obras de infraestructura", "Capacitación"];

    var user = User.get({
            username : $scope.username
    }, function(){
        if($routeParams._id){
            $scope.editando = false;
            $scope.solicitud = Solicitud.get({_id: $routeParams._id}, function(){
                $scope.solicitud.ofertas.forEach(function(o) {
                    var ofer = Oferta.get({_id:o});
                    $scope.ofertasAsignadas.push(ofer);
                });
                if(($scope.solicitud.username == $scope.username) || ($scope.hasPermission('solicitudes.admin'))) {
                    $scope.bool = true;
                }else{
                    $scope.bool = false;
                }
            });
        } else {
            $scope.solicitud = new Solicitud({
                nombre: user.nombre,
                apellido: user.apellidos,
                correo: user.email,
                entidad: user.entidad,
                comentarios: [],
                ofertas: [],
                municipios: [],
                oferta: $routeParams.oferta,
                username: $scope.username,
                municipio: "",
                estado: 'Solicitado'
            });
            $scope.editando = true;
            $scope.bool = true;
        }
    });
    
    
    
    
    
    $scope.operaciones = OfertaOperacion.query({eliminado: JSON.stringify({$exists: false})});
    $scope.organismos = OfertaOrganismo.query({eliminado: JSON.stringify({$exists: false})});
    $scope.organismosProvinciales = SolicitudOrganismoProvincial.query({eliminado: JSON.stringify({$exists: false})});
    $scope.jurisdicciones = OfertaJurisdiccion.query({eliminado: JSON.stringify({$exists: false})});
    $scope.programas = OfertaPrograma.query({eliminado: JSON.stringify({$exists: false})});

    
    $scope.volver = function() {
        $location.path('/solicitudes');
    };
    
    //Agregar municipios
    $scope.agregarMunicipio = function(dataMunicipio) {
        if (!$scope.solicitud.municipios) {
            $scope.solicitud.municipios = [];
        }
        $scope.solicitud.municipios.push(dataMunicipio);
        $scope.dataMunicipio = "";
    };
    
    //Agregar inversion
    $scope.agregarInversion = function(dataInversion) {
        if (!$scope.solicitud.inversionProyecto) {
            $scope.solicitud.inversionProyecto = [];
        }
        $scope.solicitud.inversionProyecto.push(dataInversion);
        $scope.dataInversion = "";
    };
    
    $scope.municipioPorId = function (id) {
        for (var i = 0; i < $scope.municipios.length; i++) {
            if ($scope.municipios[i]._id == id) {
                return $scope.municipios[i].nombre;
            }
        }  
    };
    
    //FILTRO QUE SACA DEL SELECT DE MUNICIPIOS LAS QUE YA FUERON SELECCIONADAS
    $scope.filtroMunicipio = function(c) {
        if($scope.solicitud) {
            if ($scope.solicitud.municipios) {
                return ($scope.solicitud.municipios.indexOf(c._id) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.filtroInversiones = function(c) {
        if($scope.solicitud) {
            if ($scope.solicitud.inversionProyecto) {
                return ($scope.solicitud.inversionProyecto.indexOf(c) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.organismoPorId = function (id) {
        for (var i = 0; i < $scope.organismos.length; i++) {
            if ($scope.organismos[i]._id == id) {
                return $scope.organismos[i].nombre;
            }
        }  
    };
    
    $scope.organismoProvincialPorId = function (id) {
        for (var i = 0; i < $scope.organismosProvinciales.length; i++) {
            if ($scope.organismosProvinciales[i]._id == id) {
                return $scope.organismosProvinciales[i].nombre;
            }
        }  
    };
    
    $scope.operacionPorId = function (id) {
        for (var i = 0; i < $scope.operaciones.length; i++) {
            if ($scope.operaciones[i]._id == id) {
                return $scope.operaciones[i].nombre;
            }
        }  
    };
    
    $scope.jurisdiccionPorId = function (id) {
        for (var i = 0; i < $scope.jurisdicciones.length; i++) {
            if ($scope.jurisdicciones[i]._id == id) {
                return $scope.jurisdicciones[i].nombre;
            }
        }  
    };
    
    $scope.programaPorId = function (id) {
        for (var i = 0; i < $scope.programas.length; i++) {
            if ($scope.programas[i]._id == id) {
                return $scope.programas[i].nombre;
            }
        }  
    };
    

    $scope.tab = 'identificacion';
    
    //FILTRO QUE SACA DEL SELECT DE TAGS LAS QUE YA FUERON SELECCIONADAS
    $scope.filtroTag = function(c) {
        if($scope.solicitud) {
            if ($scope.solicitud.tags) {
                return ($scope.solicitud.tags.indexOf(c._id) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.agregarComentario = function(confirmado, data) {
        if(confirmado){
            $scope.solicitud.comentarios.push(data);
            $scope.solicitud.estado = "Observado";
            $scope.solicitud.$save();
        }else{
            $scope.data = {
                referencia: '',
                accion: '',
                usuario: $scope.username,
                fechaComentario: new Date()
            };
            $("#agregarComentario").modal('show');
        }
    };
    
    $scope.modalPrograma = function(o) {
        $scope.oferta = Oferta.get({_id:o._id});
        $("#modalPrograma").modal('show');
    };
    
    $scope.asignarPrograma = function(confirmado, id) {
        if(confirmado){
            $scope.solicitud.ofertas.push(id);
            $scope.solicitud.$save({}, function(){
                var oferta = Oferta.get({_id:id});
                $scope.ofertasAsignadas.push(oferta);
            });
        }else{
            if (!$scope.solicitud.ofertas) {
                $scope.solicitud.ofertas = [];
            }
            $scope.ofertas = Oferta.query({}, function(){
                $scope.ofertas.forEach(function(o) {
        			o.jurisdiccionTexto = $scope.jurisdiccionPorId(o.jurisdiccion);
        		});
                $("#asignarPrograma").modal('show');
            });
        }
    };

    $scope.editar = function() {
        $scope.editando = true;
        $scope.solicitud = angular.copy($scope.solicitud);
    };

    $scope.guardar = function(){
        if(!$scope.solicitud.inversionProyecto) {
            alert("Debes seleccionar Inversión del Proyecto");
            return;
        }
        if (!$scope.solicitud.archivos) {
            $scope.solicitud.archivos = [];
        }
        if(!$scope.solicitud.nombre) {
            alert("Debes ingresar un nombre de contacto");
            return;
        }
        if (!user.interno && ($scope.solicitud.estado != "Solicitado")) {
            $scope.solicitud.estado = "Pendiente";
        }
        if ($scope.adjunto.length) {
			$scope.adjunto.forEach(function(arch) {
    			$scope.solicitud.archivos.push({
    				archivoId : arch.id,
    				nombreArchivo : arch.nombreArchivo,
    				fecha : new Date(),
    				usuario : $scope.username
    			});
			});
		}
        if ($scope.adjuntoPriorizada.length) {
			$scope.solicitud.adjuntoPriorizada = $scope.adjuntoPriorizada[0];
		}

        if($scope.solicitud._id){
                $scope.solicitud.$save(function(){
                    $scope.editando = false;
                    $scope.adjunto = [];
                    $scope.adjuntoPriorizada = [];
                    angular.extend($scope.$parent.solicitud, $scope.solicitud);
                    delete $scope.solicitud;
                });
        }else{
            $scope.solicitud.$save(function() {
                $scope.adjunto = [];
                $scope.adjuntoPriorizada = [];
                $location.path('/solicitudes/detalle/' + $scope.solicitud._id);
            });
        }
    };
    
    $scope.dameFecha = function (desde) {
        var fecha = new Date(desde);
        var devolver = fecha.getDate() + "/";
        if ((fecha.getMonth() + 1) < 10) {
            devolver = devolver + "0" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " a las " + fecha.getHours() + ":";
        } else {
            devolver = devolver + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " a las " + fecha.getHours() + ":";
        }
        if (fecha.getMinutes() < 10) {
            devolver = devolver + "0" + fecha.getMinutes() + "hs";
        } else {
            devolver = devolver + fecha.getMinutes() + "hs";
        }
        return devolver;
    };


    $scope.cancelar = function() {
        if ($scope.solicitud._id) {
            $scope.solicitud = Solicitud.get({_id: $scope.solicitud._id});
        }
    };

    $scope.eliminarListaElem = function(elemento, lista) {
        lista.splice(lista.indexOf(elemento), 1);
    };

    $scope.uploaded = [];

    var aMilisegundos = function(fecha) {
        if (fecha) {
            var fechaDividida = fecha.split("/");
            var date = new Date(fechaDividida[2], fechaDividida[1] - 1, fechaDividida[0], 0, 0, 0, 0);
            return date.getTime();
        } else {
            return 0;
        }
    };

    $scope.municipios = Municipio.query();
    
    $scope.calcularFecha = function(fecha){
        return moment(fecha).format('DD/MM/YYYY HH:mm')
    };

    $scope.existeId = function(array) { //i.roles Si id del seleccionado existe dentro del array    
        try{
            for(var i=0;i<=array.length; i++) {
                if(array[i].valor==$routeParams._id) {
                    return true;
                }
            }
            return false;
        }catch(err) {
            return false;
        }
    };

    $scope.eliminar = function(confirmado) {
        if(confirmado){
            $('#modalEliminar').modal('hide');	
            $("#modalEliminar").on('hidden.bs.modal', function () {
                $scope.solicitud.eliminado = true;
                $scope.solicitud.$delete(function(){
                    $location.path('/solicitudes');
                });
            });
        }else{
            $("#modalEliminar").modal('toggle');
        }
    };

    $scope.eliminarPrograma = function(o) {
        if (confirm("Está seguro de eliminar esta asignación?")) {
            $scope.solicitud.ofertas.splice($scope.solicitud.ofertas.indexOf(o._id), 1);
            $scope.solicitud.$save({}, function(){
                $scope.oferta = null;
                $scope.ofertasAsignadas = [];
                $scope.solicitud.ofertas.forEach(function(o) {
                    var ofer = Oferta.get({_id:o});
                    $scope.ofertasAsignadas.push(ofer);
                });
            });
        }
    };

    $scope.miliAFecha = function(mili) {
        var date = new Date(mili);
        var mes = date.getMonth()+1;
        return (date.getDate() + "/" + mes + "/" + date.getFullYear());
    };

    $scope.findById = function (lista, id) {
        if (lista && lista.length && id) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i]._id == id) {
                    return lista[i];
                }
            }
        }
    };

});