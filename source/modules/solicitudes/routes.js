exports = module.exports = {
    '/solicitudes': {
        section: 'solicitudes',
        title: 'Solicitudes',
        reloadOnSearch: false,
        allowed: ['solicitudes'],
        views: {
            'body@': {
                templateUrl: '/views/solicitudes/solicitudes.html',
            },
            'navbar-extra-left@': {
                templateUrl: '/views/solicitudes/navbar.html'
            },
            'navbar-extra-right@': {
                templateUrl: '/views/solicitudes/navbar-right.html'
            }
        }
    },
    '/solicitudes/nuevo': {
        section: 'solicitudes',
        title: 'Nueva solicitud',
        allowed: ['solicitudes'],
        edit: true,
        new: true,
        parents: ['/solicitudes'],
        views: {
            'body@': {
                templateUrl: '/views/solicitudes/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/solicitudes/navbar.html'
            }
        }
    },
    '/solicitudes/detalle/:_id': {
        section: 'solicitudes',
        title: 'Solicitud',
        reloadOnSearch: false,
        parents: ['/solicitudes'],
        views: {
            'body@': {
                templateUrl: '/views/solicitudes/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/solicitudes/navbar.html'
            }
        }
    },
    '/solicitudes/configuracion': {
        section: 'solicitudes',
        title: 'Configuración solicitud',
        allowed: ['solicitudes'],
        edit: true,
        new: true,
        parents: ['/solicitudes'],
        views: {
            'body@': {
                templateUrl: '/views/solicitudes/configuracion.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/solicitudes/navbar.html'
            }
        }
    }
};