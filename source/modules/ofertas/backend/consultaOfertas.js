exports = module.exports = function (app, options, db) {
    var doAll = function (db, res, nombresJurisdicciones, nombresSectores, nombresOrganismos) {

        var sectorPorId = function (id) {
            for (var i = 0; i < nombresSectores.length; i++) {
                if (nombresSectores[i]._id == id) {
                    return nombresSectores[i];
                }
            }
        };

        var jurisdiccionPorId = function (id) {
            for (var i = 0; i < nombresJurisdicciones.length; i++) {
                if (nombresJurisdicciones[i]._id == id) {
                    return nombresJurisdicciones[i];
                }
            }
        };

        var organismoPorId = function (id) {
            for (var i = 0; i < nombresOrganismos.length; i++) {
                if (nombresOrganismos[i]._id == id) {
                    return nombresOrganismos[i];
                }
            }
        };

        var all = [];
        db.collection('ofertas').find({}).each(function (err, item) {
            if (err) {
                res.status(503);
                console.log(err);
                return res.end();
            }

            if (item) {
                all.push({
                    "jurisdiccion": item.jurisdiccion && jurisdiccionPorId(item.jurisdiccion) && jurisdiccionPorId(item.jurisdiccion).nombre,
                    "operacion": item.operacion,
                    "programa": item.programa,
                    "objeto": item.objeto,
                    "organismo": item.organismo && organismoPorId(item.organismo) && organismoPorId(item.organismo).nombre,
                    "sector": item.sector && sectorPorId(item.sector) && sectorPorId(item.sector).nombre,
                    "objetivoEspecifico": item.objetivoEspecifico || '',
                    "saldoValor": item.saldoValor,
                    "saldoFecha": item.saldoFecha,
                    "fechaFinalizacion": item.fechaFinalizacion,
                    "nombreContacto": item.nombre,
                    "apellidoContacto": item.apellido,
                    "correoContacto": item.correo,
                    "cargoContacto": item.cargo,
                });
            }
            else {
                res.json(all);
            }
        });
    };
    
    app.get('/api/consultaOfertas', function (req, res) {
        db.collection('ofertas.jurisdicciones').find({}).toArray(function (err, nombresJurisdicciones) {
            db.collection('ofertas.sectores').find({}).toArray(function (err, nombresSectores) {
                db.collection('ofertas.organismos').find({}).toArray(function (err, nombresOrganismos) {
                    doAll(db, res, nombresJurisdicciones, nombresSectores, nombresOrganismos);
                });
            });
        });
    });
};