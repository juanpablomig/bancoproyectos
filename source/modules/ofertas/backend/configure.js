exports = module.exports = function (app, conf, db) {

    // configurar la api para descargar los contactos como vcf
    require('./vcf.js')(app, conf, db);

    // CSV de contactos
    require('express-csv');
    
    require('./consultaOfertas')(app, conf, db);  
    
    require('./consultaSolicitudes')(app, conf, db);  
};
