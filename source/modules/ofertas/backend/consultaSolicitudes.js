exports = module.exports = function (app, options, db) {
    var doAll = function (db, res, nombresMunicipios) {

        var municipioPorId = function (id) {
            for (var i = 0; i < nombresMunicipios.length; i++) {
                if (nombresMunicipios[i]._id == id) {
                    return nombresMunicipios[i];
                }
            }
        };

        var listaMunicipios = function (lista) {
            var todos = [];
            if (lista) {
                for (var i = 0; i < lista.length; i++) {
                    if (lista[i]) {
                        todos.push(municipioPorId(lista[i]).nombre) ;
                    }
                }
            }
            return todos;
        };

        var all = [];
        db.collection('solicitudes').find({}).each(function (err, item) {
            if (err) {
                res.status(503);
                console.log(err);
                return res.end();
            }

            if (item) {
                all.push({
                    "id": item._id || '',
                    "denominacion": item.denominacion || '',
                    "descripcion": item.descripcion || '',
                    "beneficios": item.beneficios || '',
                    "organismoEjecutor": item.organismoE || '',
                    "estadoProyecto": item.estadoProyecto || '',
                    "solicitud": item.solicitud || '',
                    "problema": item.problema || '',
                    "municipios": listaMunicipios(item.municipios),
                    "estado": item.estado || '',
                });
            }
            else {
                res.json(all);
            }
        });
    };
    
    app.get('/api/consultaSolicitudes', function (req, res) {
        db.collection('municipios').find({}).toArray(function (err, nombresMunicipios) {
            doAll(db, res, nombresMunicipios);
        });
    });
};