angular.module('bag2.ofertas', ['service-api'])
.controller('OfertaCtrl', function($scope, $routeParams, Oferta, OfertaJurisdiccion, OfertaOrganismo, OfertaSector, OfertaGasto, OfertaComponente, Solicitud, Municipio, $location) { //Lista de ofertas
    
    
    $scope.jurisdicciones = OfertaJurisdiccion.query({eliminado: JSON.stringify({$exists: false})}, function(){
        $scope.sectores = OfertaSector.query({eliminado: JSON.stringify({$exists: false})}, function(){
            $scope.municipios = Municipio.list();
            $scope.solicitudes = Solicitud.query();
            $scope.componentes = OfertaComponente.query({eliminado: JSON.stringify({$exists: false})});
            $scope.organismos = OfertaOrganismo.query({eliminado: JSON.stringify({$exists: false})});
            $scope.gastos = OfertaGasto.query({eliminado: JSON.stringify({$exists: false})});
            
            //Listado de ofertas cargados
            $scope.ofertas = Oferta.query({}, function(){
                $scope.ofertas.forEach(function(o) {
        			o.sectorTexto = $scope.sectorPorId(o.sector);
        			o.organismoTexto = $scope.organismoPorId(o.organismo);
        			o.jurisdiccionTexto = $scope.jurisdiccionPorId(o.jurisdiccion);
        			o.estadoNumero = $scope.ordenEstado(o.estado);
        		});
            });
        });
    });
    
    $scope.orden = "estadoNumero";
    
    $scope.ordenEstado = function (i) {
        if (i == "Activo") {
            return 1;
        } else if (i == "Vencido") {
            return 2;
        } else if (i == "Comprometido") {
            return 3;
        } else if (i == "Inactiva") {
            return 4;
        }
    };
    
    $scope.solicitudesPorOferta = function (id) {
        var cantidad = 0;
        for (var i = 0; i < $scope.solicitudes.length; i++) {
            if ($scope.solicitudes[i].ofertas) {
                if ($scope.solicitudes[i].ofertas.indexOf(id) > -1) {
                    cantidad = cantidad + 1;
                }
            }
        }
        return cantidad;
    };
    
    
    /*$scope.acomodarBase = function () {
        $scope.operaciones = OfertaOperacion.query({eliminado: JSON.stringify({$exists: false})}, function(){
            $scope.programas = OfertaPrograma.query({eliminado: JSON.stringify({$exists: false})}, function(){
                $scope.ofertas.forEach(function(ofe) {
                    if (ofe.operacion) {
            			ofe.operacion = $scope.operacionPorId(ofe.operacion);
                    }
                    if (ofe.programa) {
            			ofe.programa = $scope.programaPorId(ofe.programa);
                    }
                    ofe.$save();
        		});
            });
        });
    };*/
    
    $scope.colorLinea = function(i){
        if (!i.estado) {
            return {'border-bottom':'3px solid #999'};
        } else {
            if (i.estado == 'Vencido') {
                return {'border-bottom':'3px solid #f0ad4e'};
            } else if (i.estado == 'Comprometido') {
                return {'border-bottom':'3px solid #d9534f'};
            } else if (i.estado == 'Activo') {
                return {'border-bottom':'3px solid #5cb85c'};
            } else if (i.estado == 'Inactiva') {
                return {'border-bottom':'3px solid #999'};
            }
        }
    };
    
    $scope.jurisdiccionPorId = function (id) {
        for (var i = 0; i < $scope.jurisdicciones.length; i++) {
            if ($scope.jurisdicciones[i]._id == id) {
                return $scope.jurisdicciones[i].nombre;
            }
        }  
    };
    
    $scope.organismoPorId = function (id) {
        for (var i = 0; i < $scope.organismos.length; i++) {
            if ($scope.organismos[i]._id == id) {
                return $scope.organismos[i].nombre;
            }
        }  
    };
    
    $scope.sectorPorId = function (id) {
        for (var i = 0; i < $scope.sectores.length; i++) {
            if ($scope.sectores[i]._id == id) {
                return $scope.sectores[i].nombre;
            }
        }  
    };
    
    /*$scope.operacionPorId = function (id) {
        for (var i = 0; i < $scope.operaciones.length; i++) {
            if ($scope.operaciones[i]._id == id) {
                return $scope.operaciones[i].nombre;
            }
        }  
    };
    
    $scope.programaPorId = function (id) {
        for (var i = 0; i < $scope.programas.length; i++) {
            if ($scope.programas[i]._id == id) {
                return $scope.programas[i].nombre;
            }
        }  
    };*/
    
    $scope.municipioPorId = function (id) {
        for (var i = 0; i < $scope.municipios.length; i++) {
            if ($scope.municipios[i]._id == id) {
                return $scope.municipios[i].nombre;
            }
        }  
    };
    
    
    $scope.exportToExcel=function(){
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "text/html; charset=UTF-8"
        });
        saveAs(blob, "Ofertas.xls");
    };
    
    $scope.reordenar = function(texto) {
        if (texto == $scope.orden)
            return ('-' + texto);
        else
            return texto;
    };
    
    $scope.verOferta = function(id) {
        $location.path('/ofertas/detalle/' + id);
    };
    
})

.controller('OfertaDetalleCtrl',function($scope,$location,$routeParams,API,User,Oferta, Solicitud, OfertaJurisdiccion, OfertaGasto, OfertaOrganismo, OfertaOperacion, OfertaPrograma, OfertaSector, OfertaObjetivo, OfertaComponente, Municipio){

    $scope.solicitudes = [];
    $scope.adjunto = [];
    $scope.listaInversiones = ["Consultoría o asistencia técnica", "Equipamiento: equipamiento tecnológico", "Equipamiento: bienes de capital", "Equipamiento: mobiliario", "Obras de infraestructura", "Capacitación"];
    
    if($routeParams._id){
        $scope.editando = false;
        $scope.oferta = Oferta.get({_id: $routeParams._id}, function(){
            var solicitudes = Solicitud.query({}, function(){
                solicitudes.forEach(function(sol) {
                    if (sol.ofertas) {
            			if (sol.ofertas.indexOf($scope.oferta._id) > -1) {
            			    $scope.solicitudes.push(sol);
            			}
                    }
    			});
            });
        });

        // Traigo el user completo con el username.
        var user = User.get({
            username : $scope.username
        }, function(){
            if(user.idOferta == $routeParams._id) {
                $scope.bool = true;
            }else{
                $scope.bool = false;
            }
        });
    } else {
        $scope.editando = true;
        $scope.oferta = new Oferta({
            municipios: ["60897a8d8d10f83545f3afc6"],
            objetivos: [],
            componentes: [],
            objeto: "",
            programa: "",
            operacion: "",
            organismo: "",
            sector: "",
            jurisdiccion: "",
            gastoElegible: "",
            estado: ''
        });
    }
    
    $scope.componentes = OfertaComponente.query({eliminado: JSON.stringify({$exists: false})});
    $scope.objetivos = OfertaObjetivo.query({eliminado: JSON.stringify({$exists: false})});
    $scope.sectores = OfertaSector.query({eliminado: JSON.stringify({$exists: false})});
    /*$scope.programas = OfertaPrograma.query({eliminado: JSON.stringify({$exists: false})});
    $scope.operaciones = OfertaOperacion.query({eliminado: JSON.stringify({$exists: false})});*/
    $scope.organismos = OfertaOrganismo.query({eliminado: JSON.stringify({$exists: false})});
    $scope.jurisdicciones = OfertaJurisdiccion.query({eliminado: JSON.stringify({$exists: false})});
    $scope.gastos = OfertaGasto.query({eliminado: JSON.stringify({$exists: false})});
    $scope.municipios = Municipio.list();
    
    $scope.volver = function() {
        $location.path('/ofertas');
    };
    
    $scope.aBlanco = function(fecha, id) {
        fecha = "";
        $(id).val("");
    };
    
    $scope.tab = 'identificacion';
    
    //FILTRO QUE SACA DEL SELECT DE MUNICIPIOS LAS QUE YA FUERON SELECCIONADAS
    $scope.filtroMunicipio = function(c) {
        if($scope.oferta) {
            if ($scope.oferta.municipios) {
                return ($scope.oferta.municipios.indexOf(c._id) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.filtroInversiones = function(c) {
        if($scope.oferta) {
            if ($scope.oferta.inversionProyecto) {
                return ($scope.oferta.inversionProyecto.indexOf(c) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.filtroGasto = function(c) {
        if($scope.oferta) {
            if ($scope.oferta.gastoElegible) {
                return ($scope.oferta.gastoElegible.indexOf(c._id) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.filtroComponente = function(c) {
        if($scope.oferta) {
            if ($scope.oferta.componentes) {
                return ($scope.oferta.componentes.indexOf(c._id) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.filtroObjetivo = function(c) {
        if($scope.oferta) {
            if ($scope.oferta.objetivos) {
                return ($scope.oferta.objetivos.indexOf(c._id) == -1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    };
    
    $scope.municipioPorId = function (id) {
        for (var i = 0; i < $scope.municipios.length; i++) {
            if ($scope.municipios[i]._id == id) {
                return $scope.municipios[i].nombre;
            }
        }  
    };
    
    $scope.objetivoPorId = function (id) {
        for (var i = 0; i < $scope.objetivos.length; i++) {
            if ($scope.objetivos[i]._id == id) {
                return $scope.objetivos[i].nombre;
            }
        }  
    };
    
    $scope.organismoPorId = function (id) {
        for (var i = 0; i < $scope.organismos.length; i++) {
            if ($scope.organismos[i]._id == id) {
                return $scope.organismos[i].nombre;
            }
        }  
    };
    
    $scope.componentePorId = function (id) {
        for (var i = 0; i < $scope.componentes.length; i++) {
            if ($scope.componentes[i]._id == id) {
                return $scope.componentes[i].nombre;
            }
        }  
    };
    
    $scope.gastoPorId = function (id) {
        for (var i = 0; i < $scope.gastos.length; i++) {
            if ($scope.gastos[i]._id == id) {
                return $scope.gastos[i].nombre;
            }
        }  
    };
    
    $scope.jurisdiccionPorId = function (id) {
        for (var i = 0; i < $scope.jurisdicciones.length; i++) {
            if ($scope.jurisdicciones[i]._id == id) {
                return $scope.jurisdicciones[i].nombre;
            }
        }  
    };
    
    /*$scope.operacionPorId = function (id) {
        for (var i = 0; i < $scope.operaciones.length; i++) {
            if ($scope.operaciones[i]._id == id) {
                return $scope.operaciones[i].nombre;
            }
        }  
    };
    
    $scope.programaPorId = function (id) {
        for (var i = 0; i < $scope.programas.length; i++) {
            if ($scope.programas[i]._id == id) {
                return $scope.programas[i].nombre;
            }
        }  
    };*/
    
    //Agregar municipios
    $scope.agregarMunicipio = function(dataMunicipio) {
        if (!$scope.oferta.municipios) {
            $scope.oferta.municipios = [];
        }
        $scope.oferta.municipios.push(dataMunicipio);
        $scope.dataMunicipio = "";
    };
    
    //Agregar inversion
    $scope.agregarInversion = function(dataInversion) {
        if (!$scope.oferta.inversionProyecto) {
            $scope.oferta.inversionProyecto = [];
        }
        $scope.oferta.inversionProyecto.push(dataInversion);
        $scope.dataInversion = "";
    };
    
    //Agregar gasto
    $scope.agregarGasto = function(dataGasto) {
        if (!$scope.oferta.gastoElegible) {
            $scope.oferta.gastoElegible = [];
        }
        $scope.oferta.gastoElegible.push(dataGasto);
        $scope.dataGasto = "";
    };
    
    //Agregar objetivos
    $scope.agregarObjetivo = function(dataObjetivo) {
        if (!$scope.oferta.objetivos) {
            $scope.oferta.objetivos = [];
        }
        $scope.oferta.objetivos.push(dataObjetivo);
        $scope.dataObjetivo = "";
    };
    
    //Agregar componentes
    $scope.agregarComponente = function(dataComponente) {
        if (!$scope.oferta.componentes) {
            $scope.oferta.componentes = [];
        }
        $scope.oferta.componentes.push(dataComponente);
        $scope.dataComponente = "";
    };

    $scope.editar = function() {
        $scope.editando = true;
        $scope.oferta = angular.copy($scope.oferta);
    };

    $scope.guardar = function(){
        if(!$scope.oferta.inversionProyecto) {
            alert("Debes seleccionar Inversión del Proyecto");
            return;
        }
        if (!$scope.oferta.archivos) {
            $scope.oferta.archivos = [];
        }
        if ($scope.adjunto.length) {
			$scope.adjunto.forEach(function(arch) {
    			$scope.oferta.archivos.push({
    				archivoId : arch.id,
    				nombreArchivo : arch.nombreArchivo,
    				fecha : new Date(),
    				usuario : $scope.username
    			});
			});
		}
        if($routeParams._id){
            $scope.oferta.$save(function(){
                $scope.editando = false;
                $scope.adjunto = [];
                angular.extend($scope.$parent.oferta, $scope.oferta);
                delete $scope.oferta;
            });
        }else{
            $scope.oferta.$save(function() {
                $scope.adjunto = [];
                $location.path('/ofertas/detalle/' + $scope.oferta._id);
            });
        }
    };
    
    $scope.dameFecha = function (desde) {
        var fecha = new Date(desde);
        var devolver = fecha.getDate() + "/";
        if ((fecha.getMonth() + 1) < 10) {
            devolver = devolver + "0" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " a las " + fecha.getHours() + ":";
        } else {
            devolver = devolver + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " a las " + fecha.getHours() + ":";
        }
        if (fecha.getMinutes() < 10) {
            devolver = devolver + "0" + fecha.getMinutes() + "hs";
        } else {
            devolver = devolver + fecha.getMinutes() + "hs";
        }
        return devolver;
    };

    $scope.cancelar = function() {
        $scope.oferta = Oferta.get({_id: $routeParams._id});
    };

    $scope.eliminarListaElem = function(elemento, lista) {
        lista.splice(lista.indexOf(elemento), 1);
    };

    $scope.ofertas = Oferta.query();

    $scope.auxiliar = Oferta.query({});

    $scope.existeId = function(array) { //i.roles Si id del seleccionado existe dentro del array    
        try{
            for(var i=0;i<=array.length; i++) {
                if(array[i].valor==$routeParams._id) {
                    return true;
                }
            }
            return false;
        }catch(err) {
            return false;
        }
    };
    
    $scope.modalSolicitud = function(soli) {
        $scope.soli = soli;
        $("#modalSolicitud").modal('show');
    };

    $scope.eliminar = function(confirmado) {
        if(confirmado){
            $('#modalEliminar').modal('hide');	
            $("#modalEliminar").on('hidden.bs.modal', function () {
                $scope.oferta.eliminado = true;
                $scope.oferta.$delete(function(){
                    $location.path('/ofertas');
                });
            });
        }else{
            $("#modalEliminar").modal('toggle');
        }
    };

    $scope.miliAFecha = function(mili) {
        var date = new Date(mili);
        var mes = date.getMonth()+1;
        return (date.getDate() + "/" + mes + "/" + date.getFullYear());
    };

    $scope.findById = function (lista, id) {
        if (lista && lista.length && id) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i]._id == id) {
                    return lista[i];
                }
            }
        }
    };

});