exports = module.exports = {
    '/ofertas': {
        section: 'ofertas',
        title: 'Ofertas',
        reloadOnSearch: false,
        allowed: ['ofertas'],
        views: {
            'body@': {
                templateUrl: '/views/ofertas/ofertas.html',
            },
            'navbar-extra-left@': {
                templateUrl: '/views/ofertas/navbar.html'
            },
            'navbar-extra-right@': {
                templateUrl: '/views/ofertas/navbar-right.html'
            }
        }
    },
    '/ofertas/nuevo': {
        section: 'ofertas',
        title: 'Nueva oferta',
        allowed: ['ofertas'],
        edit: true,
        new: true,
        parents: ['/ofertas'],
        views: {
            'body@': {
                templateUrl: '/views/ofertas/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/ofertas/navbar.html'
            }
        }
    },
    '/ofertas/detalle/:_id': {
        section: 'ofertas',
        title: 'Oferta',
        reloadOnSearch: false,
        parents: ['/ofertas'],
        views: {
            'body@': {
                templateUrl: '/views/ofertas/detalle.html'
            },
            'navbar-extra-left@': {
                templateUrl: '/views/ofertas/navbar.html'
            }
        }
    }
};