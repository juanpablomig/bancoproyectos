exports = module.exports = [{
  name: 'Ofertas - Acceso',
  key: 'ofertas'
},{
  name: 'Ofertas - Edición',
  key: 'ofertas.editar'
},{
  name: 'Ofertas - Acceso BD',
  key: 'ofertas.db'
},{
  name: 'Ofertas - Solicitante',
  key: 'ofertas.solicitante'
},{
  name: 'Ofertas - Eliminar',
  key: 'ofertas.eliminar'
}];