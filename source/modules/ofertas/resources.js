exports = module.exports = [{
    name: 'Oferta',
    collectionName: 'ofertas',
    url: '/ofertas/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas'],
            allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas','/ofertas/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaJurisdiccion',
    collectionName: 'ofertas.jurisdicciones',
    url: '/ofertas.jurisdicciones/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.jurisdicciones'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.jurisdicciones','/ofertas.jurisdicciones/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaOrganismo',
    collectionName: 'ofertas.organismos',
    url: '/ofertas.organismos/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.organismos'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.organismos','/ofertas.organismos/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaGasto',
    collectionName: 'ofertas.gastos',
    url: '/ofertas.gastos/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.gastos'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.gastos','/ofertas.gastos/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaOperacion',
    collectionName: 'ofertas.operaciones',
    url: '/ofertas.operaciones/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.operaciones'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.operaciones','/ofertas.operaciones/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaPrograma',
    collectionName: 'ofertas.programas',
    url: '/ofertas.programas/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.programas'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.programas','/ofertas.programas/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaSector',
    collectionName: 'ofertas.sectores',
    url: '/ofertas.sectores/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.sectores'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.sectores','/ofertas.sectores/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaObjetivo',
    collectionName: 'ofertas.objetivos',
    url: '/ofertas.objetivos/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.objetivos'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.objetivos','/ofertas.objetivos/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
},{
    name: 'OfertaComponente',
    collectionName: 'ofertas.componentes',
    url: '/ofertas.componentes/:_id',
    params: {
        _id: '@_id'
    },
    actions: {
        list: {
            urls: ['/ofertas.componentes'],
            //allowed: ['ofertas.db'],
            kind: 'find'
        },
        findById: {
            kind: 'findOne',
            allowed: ['ofertas.db']
        },
        save: {
            urls: ['/ofertas.componentes','/ofertas.componentes/:_id'],
            allowed: ['ofertas.db'],
            kind: 'findAndModify'
        },
        delete: {
            kind: 'remove',
            allowed: ['ofertas']
        }
    }
}];