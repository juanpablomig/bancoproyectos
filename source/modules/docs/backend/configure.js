exports = module.exports = function (app, conf, db) {
    var Grid = require('gridfs');
    var gfs = Grid(db, require('mongodb'));

    app.post('/upload/*', function (req, res, next) {

        try {
            var name = req.files.qqfile.name;
            var ext = name.slice(name.lastIndexOf('.'));
            var id = require('shortid').generate() + ext;
            var data = req.files.qqfile.data;

            gfs.writeFile({ filename: id, metadata: { name: name } }, data, function (err) {
                if (err) return next(err);

                res.json({ ok: true, id: id, success: true });
            });

        } catch (e) {
            next(e);
        }
    });

    app.get('/file/:id', function (req, res, next) {
        var id = req.params.id;

        gfs.exist({
            filename: id
        }, function (err, found) {
            if (err) return next(err);

            if (!found) {
                res.send(404);
                res.end();

                return;
            }

            gfs.findOne({ filename: id }, function (err, file) {
                gfs.readFile({ filename: id }, function (err, data) {
                    if (err) return next(err);
                    var name = file.metadata && file.metadata.name ? file.metadata.name : id;

                    res.writeHead(200, {
                        'Content-Type': 'application/octet-stream',
                        "content-disposition": "attachment; filename=\"" + name + "\""
                    });
                    res.write(data);

                    res.end();
                });
            });
        });
    });
};