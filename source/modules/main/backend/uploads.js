exports = module.exports = function (app, conf, db) {
    var path = require('path');

    app.post('/api/files/upload', function (req, res, next) {
        if (!req.files.file.qqfile) return next(new Error('unknown file'));
       
        var mongodb = require('mongodb');
        var oid = new mongodb.ObjectID();
        var gs = new mongodb.GridStore(db, oid.toString(), "w", {});
        gs.open(function (err, gs) {
            if (err) {
                console.log(err);
                res.status(503);
                res.end();
            }
            else {
                // debugger;
                gs.writeFile(uploadPath, function (err, gs) {
                    if (err) {
                        console.log(err);
                        res.status(503);
                        res.end();
                    }
                    else {
                        gs.close();
                        res.json({
                            ok: true,
                            id: oid.toString(),
                            success: true
                        });
                    }
                });
            }
        });
    });
    app.get('/api/upload/:id', function (req, res) {
        var mongodb = require('mongodb');
        var gs = new mongodb.GridStore(db, req.params.id, "r", {});
        gs.open(function (err, gs) {
            if (gs) {
                gs.read(function (err, data) {
                    if (err) {
                        console.log(err);
                        res.status(503);
                        res.end();
                    }
                    else {
                        res.setHeader('Cache-Control', 'max-age=28800');
                        res.write(data);
                        res.end();
                    }
                });
            }
            else {
                res.status(404);
                res.end();
            }
        });
    });
};