exports = module.exports = function(app, conf, db) {
    require('./index')(app, conf, db);
    require('./uploads')(app, conf, db);
    require('./login')(app, conf, db);
    require('./angulartics')(app, conf, db);
};
