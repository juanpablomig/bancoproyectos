exports = module.exports = {
    '/apps': {
        title: 'SOFEP - Aplicaciones',
        section: 'apps',
        views: {
            'body@': {
                templateUrl: '/views/apps.html'
            }
        }
    },
    '/login': {
        title: 'SOFEP - Ingresar',
        section: '',
        views: {
            'body@': {
                templateUrl: '/views/login.html'
            }
        }
    }
};