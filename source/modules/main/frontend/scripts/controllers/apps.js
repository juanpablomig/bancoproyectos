angular.module('bag2.apps', ["bag2.checkUrl"])
.controller('AppsCtrl', function($scope, $http, $location, $routeParams, checkUrl, getUiStates, updateRoutes, User) {
    /*updateRoutes().then(function () {
        checkUrl.checkUrl().then(function (u) {
           $location.url(u)
        });
    });*/

    
    $scope.user = User.findByName({
        username: $scope.username
    }, function(){
        if ($scope.user.interno) {
            $scope.texto = "Interno";
        } else {
            $scope.texto = "Externo";
        }
        if ($routeParams.login) {
            $("#modalPopup").modal('show');
        }
    });
    
    $scope.fitroBC = function (c) {
        if ((c.entidad == "bc") || (c.entidad == "bcfundacion")) {
            return true;
        } else {
            return false;
        }
    };
    
    $scope.fitroFundacion = function (c) {
        if ((c.entidad == "fundacion") || (c.entidad == "bcfundacion")) {
            return true;
        } else {
            return false;
        }
    };

    $http.get('/api/apps').success(function(data) {
        var appsMenu = [];
        data.forEach(function(a) {
            if (a.display === undefined || a.display === true) {
                appsMenu.push(a);
            }
        });
        $scope.apps = appsMenu;
    });
});