angular.module('bag2.navbar', []).controller('NavbarAuthCtrl', function($scope, $location, auth, User) {
    $scope.loggedIn = undefined;
    $scope.login = function() {
        $location.url('/login');
    };
    $scope.logout = function() {
        auth.logout();
    };
});
