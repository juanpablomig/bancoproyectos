    angular.module("bag2.fineUploader", [])
        .directive("fineUploader", function($compile, $interpolate) {
            return {
                restrict: "A",
                replace: true,
                scope: {
                    uploaded: '=',
                    destination: '=',
                    placeholder: '=',
                    extensions: '='
                },

                link: function($scope, element, attrs) {
                    var uploader = new qq.FineUploader({
                            element: element[0],
                            request: {endpoint: $scope.destination},          
                            validation: {
                                sizeLimit: 50000000,
                                allowedExtensions: $scope.extensions ? $scope.extensions.split(',') : '',
                            },
                            
                            text: {
                                uploadButton: '<i class="icon-upload icon-white"></i>&nbsp;' + ($scope.placeholder || 'Upload files')
                            },

                            deleteFile: {
                                endpoint: $scope.destination,
                                enabled: true
                            },

                            display: {
                                prependFiles: true
                            },

                            failedUploadTextDisplay: {
                                mode: "custom"
                            },
                            
                            retry: {
                                enableAuto: true
                            },

                            chunking: {
                                enabled: false
                            },

                            resume: {
                                enabled: true
                            },

                            callbacks: {
                                onComplete: function(id, fileName, responseJSON) {
                                    if ($scope.uploaded) {
                                        //add the new objects
                                        responseJSON.nombreArchivo = fileName;
                                        $scope.uploaded.push(responseJSON);
            
                                        //queue a digest.
                                        $scope.$apply();
                                    }
                                }
                            }
                        });
                }
            }
        });