exports = module.exports = [
    "/scripts/angulartics.js",
    "/scripts/angulartics-scroll.js",
    "/scripts/angulartics-bag2.js", //REGISTRO DE TICKS
    "/scripts/other/date.js", //FORMATOS DE FECHA
	'/scripts/groupBy.js',
    "/components/angular/angular-route.js",
    "/components/angular/angular-resource.js",
    "/components/angular/angular-cookies.js", 
    "/components/angular/angular-sanitize.js",
    //"/components/angular/angular-touch.js",
    "/components/angular/angular-animate.js",
    "/components/select2/select2.js",
    "/components/select2/select2_locale_es-min.js", 
    //"/bastrap3/bootstrap.min.js",
    "/bootstrap-3.4/js/bootstrap.min.js",
    "/components/underscore/underscore-min.js", //AGREGA FUNCIONES A JAVASCRIPT
    "/components/datetimepicker/momentjs.js", 
    "/components/angular-ui/build/angular-ui.js",
    //"/components/angular-ui-utils/modules/keypress/keypress.js", 
    "/components/ui-router/release/angular-ui-router.min.js",
    //"/components/fullcalendar/fullcalendar2.min.js", //CALENDARIOS
    "/components/angular-bootstrap/ui-bootstrap-tpls-0.12.1.min.js",
    "/components/angular-strap/modal.js", //MODALS EN HTML APARTE
    "/components/fine-uploader/jquery.fine-uploader.js", //SUBIR ARCHIVOS
    "/components/jquery-ui/ui/minified/jquery-ui.min.js", 
    "/components/datetimepicker/jquery-min.js", //CALENDARIO SELECCIONAR FECHA
    "/components/datetimepicker/directive.js", //CALENDARIO SELECCIONAR FECHA
    "/components/bootstrap-timepicker/js/bootstrap-timepicker.min.js", //SELECCIONAR HORA
    //"/components/bootstrap-colorpicker/js/bootstrap-colorpicker-min.js", //SELECCIONAR COLOR
    //"/components/bootstrap-colorpicker/js/directive.js", //SELECCIONAR COLOR
    "/scripts/services/auth.js", 
    "/components/macgyver/macgyver-min.js", //TOOLTIPS Y OTROS
    "/scripts/filters/toArray.js",
    "/scripts/filters/skip.js",
    "/scripts/controllers/login.js", 
    "/scripts/services/auth.js",
    "/scripts/controllers/alerts.js",
    "/scripts/services/checkUrl.js",
    "/scripts/directives/fineuploader.js", //SUBIR ARCHIVOS
    "/scripts/app.js",
    "/scripts/controllers/page.js",
    "/scripts/controllers/notAllowed.js", 
    "/scripts/controllers/apps.js",
    "/scripts/controllers/navbar.js",
    "/scripts/directives/route-class.js",
    "/scripts/directives/editModel.js",
    "/scripts/defaults.js",
    "/scripts/directives/scrollglue.js", 
    "/scripts/services/search.js",
    "/scripts/other/url.js",
    "/scripts/base64.js",
    "/components/angular-nvd3/d3.js", //GRAFICOS
    "/components/angular-nvd3/nv.d3.js", //GRAFICOS
    "/components/angular-nvd3/angular-nvd3.js", //GRAFICOS
    "/components/ui-select/select-min.js",
    "/components/jscroller/jscroller-0.4.js",
    "/components/cryptojs/aes.js", //ENCRIPTAR CONTRASEÑA
    "/components/file-saver/FileSaver.min.js", //EXPORTAR A EXCEL
    //"/components/angular-ui-tinymce/src/tinymce.js", //EDITOR DE TEXTO ENRIQUECIDO
    //"/components/tinymce/tinymce.js", //EDITOR DE TEXTO ENRIQUECIDO
    ];