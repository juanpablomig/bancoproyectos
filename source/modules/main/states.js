exports = module.exports = [{
        name: 'not-allowed',
        title: 'SOFEP',
        templateUrl: '/views/oneColumn.html',
        views: {
            'body@': {
                templateUrl: '/views/notAllowed.html',
            },
            'navbar-extra-left@': {},
            'navbar-extra-right@': {}
        }
    }, {
        url:'',
        name: 'dashboard',
        title: 'SOFEP',
        templateUrl: '/views/oneColumn.html',
        views: {
            'body@': {
                templateUrl: '/views/apps.html',
            }
        }
    }
];