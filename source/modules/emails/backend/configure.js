exports = module.exports = function(app, conf, db) {
    var createMessage = require('./locals').createMessage = function(from, subject, text, html, to, cc, bcc, attachment, nameAttachment) {
        if (attachment != ""){
            var Grid = require('gridfs');
            var gfs = Grid(db, require('mongodb'));
            const fs = require('fs');
            gfs.readFile({ filename: attachment }, function (err, buffer) {
                fs.writeFileSync('/tmp/archivo.algo', buffer);
            });
        }
        
        if (attachment == ""){
            var message = {
                text: text,
                from: from,
                to: to,
                cc: cc,
                bcc: bcc,
                subject: subject,
                attachment: [{
                    data: html,
                    alternative: true
                }]
            };
        } else {
            
            var message = {
                text: text,
                from: from,
                to: to,
                cc: cc,
                bcc: bcc,
                subject: subject,
                attachment: [{
                    data: html,
                    alternative: true
                },
                {
                    path: require('path').join('/tmp/archivo.algo'),
                    name:nameAttachment
                }]
            };
        }
        return message;
    };

    require('./locals').sendMail = function(message, callback) {
        var email = require('emailjs/email');
        console.log(conf);
        var server = email.server.connect({
            user: conf.email.smtp.username,
            password: conf.email.smtp.password,
            host: conf.email.smtp.host,
            port: conf.email.smtp.port,
            ssl: conf.email.smtp.useSsl,
			tls: conf.email.smtp.tls
        });

        server.send(message, function(err, message) {
            if (callback) {
                return callback(err, message);
            }
        });
    };
};