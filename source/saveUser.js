var permissions = process.argv.splice(4);
var args = process.argv.splice(2);

if (args.length < 2) {
    console.log('not enough arguments');
    console.log('saveUser username password permission1 permission2 permission3');
}

var username = args[0];
var password = args[1];

require('mongodb').MongoClient.connect(require('config').db, function (err, db) {
    if (err) {
        console.log(err);
        process.exit(-1);
        return;
    }
    var hashed = require('password-hash').generate(password);
    db.collection('users').findAndModify({
        username: username
    }, {}, {
            $set: {
                username: username,
                password: hashed
            }
        }, {
            upsert: true,
            safe: true
        }, function (err) {
            if (err) {
                console.log(err);
            }
            else {
                db.collection('users.permissions').findAndModify({
                    username: username
                }, {}, {
                        $set: {
                            username: username,
                            permissions: permissions
                        }
                    }, {
                        safe: true,
                        upsert: true
                    }, function (err) {
                        if (err) {
                            console.log(err);
                        }
                        process.exit(0);
                    });
            }
        });
});
