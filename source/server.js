require('async')
    .autoInject({
        config: function (callback) {
            callback(null, require('config'));
        },
        db: function (config, callback) {
            require('mongodb').MongoClient.connect(config.db, callback);
        },
        session: function (config, db, callback) {
            var Session = require('express-session');
            var MongoStore = require('connect-mongo')(Session);

            var store = new MongoStore({ db: db, collection: config.session.collection });

            var session = Session({
                secret: config.session.secret,
                name: config.session.cookieName,
                store: store
            });

            callback(null, session);
        },
        compression: function (callback) {
            callback(null, require('compression')());
        },
        /*bodyParser: function (callback) {
            callback(null, require('body-parser')());
        },*/
        fileUpload: function (callback) {
            callback(null, require('express-fileupload')());
        },
        app: function (session, compression, fileUpload, callback) { /*saque el parametro bodyParser*/
            var app = require('express')();

            app.use(compression);
            app.use(session);
            //app.use(bodyParser); LO PUSE EN EL CONFIGURE DE GLUE
            app.use(fileUpload);

            callback(null, app);
        }
    },
    function (err, results) {
        if (err) throw err;

        require('./glue/configure')(
            results.app,
            results.db,
            results.config,
            require('path').join(__dirname, 'modules'),
            function () {

                if (results.config.https) {
                    require('https')
                        .createServer({
                            key: require('fs').readFileSync(results.config.https),
                            cert: require('fs').readFileSync(results.config.https)
                        }, results.app)
                        .listen(results.config.http.port, results.config.http.ip, function () {
                            console.log("Listening");
                        });
                } else {
                    require('http')
                        .createServer(results.app)
                        .listen(results.config.http.port, results.config.http.ip, function () {
                            console.log("Listening");
                        });
                }
            });

    }
    );