Actualizar Gestionar
==========

1) Actualizar el repositorio, parado en la carpeta del proyecto:
	```git pull```

2) Parado en la carpeta /source borrar la carpeta node_modules:
```
cd source
rm -Rf node_modules
```

3) Parado en la carpeta /source ejecutar:
	```npm install```

4) Parado en la carpeta /source ejecutar:
```
sudo service mongod start
forever start server.js
```
