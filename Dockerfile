FROM ubuntu:16.10

RUN apt update
RUN apt install -yy nodejs npm

ADD source /src

WORKDIR /src
RUN npm install

EXPOSE 8080

CMD node server
